var CORE       = require(WPATH("core"));
var CONTACT    = require(WPATH("contact"));
var EMAIL      = require(WPATH("email"));
var PHONE      = require(WPATH("phone"));
var SMS        = require(WPATH("sms"));
var MAP        = require(WPATH("map"));

MAP.mapView = $.modMap;
SMS.init();

$.init = function(_contact){    
    $.lblFullname.text  = _contact.firstName+" "+_contact.lastName;
    
    if(typeof _contact.organization != "undefined"){
        $.lblCompany.text   = _contact.organization;
    }
    
    if(typeof _contact.addresses != "undefined"){
        setAddresses(_contact.addresses);
    }
    
    if(typeof _contact.phones != "undefined"){
        setPhones(_contact.phones);
    }
        
    if(typeof _contact.emails != "undefined"){
        setEmails(_contact.emails); 
    }
       
};

function createAddress(_address){
    var vwAddress = Ti.UI.createView({
        height:Titanium.UI.SIZE,
        layout:"vertical"
    });
    
    var vwAddressDetails = Ti.UI.createView({
        height:Titanium.UI.SIZE,
        layout:"vertical",
        touchEnable:false,
        bubbleParent:true
    });
    
    var vwAddressWrapper = Ti.UI.createView({
        height:Titanium.UI.SIZE,
        touchEnable:false,
        bubbleParent:true
    });
    
    var lblAddress1 = Ti.UI.createLabel({
        text: _address.Street,
        width:Titanium.UI.FILL,
        textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT,
        font:{
            fontSize:16,
            fontFamily: "HelveticaNeue-Light"
        },
        touchEnable:false,
        bubbleParent:true
    });
    
    var lblAddress2 = Ti.UI.createLabel({
        text: _address.City+", "+_address.ZIP+" "+_address.State,
        width:Titanium.UI.FILL,
        textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT,
        font:{
            fontSize:16,
            fontFamily: "HelveticaNeue-Light"
        },
        touchEnable:false,
        bubbleParent:true
    });    

    var lblAddress3 = Ti.UI.createLabel({
        text: _address.Country,
        width:Titanium.UI.FILL,
        textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT,
        font:{
            fontSize:16,
            fontFamily: "HelveticaNeue-Light"
        },
        touchEnable:false,
        bubbleParent:true     
    });     
    
    var imgLocate = Ti.UI.createImageView({
        image: WPATH("./images/icon_map.png"),
        top:0,
        right:5,
        width:40,
        height:40,
        touchEnable:false,
        bubbleParent:true
    });
    
    var spacer15    = _createSpacer(15);
    var seperator   = _createSeperator("#cccccc");
    var spacer5     = _createSpacer(5);
    
   
    vwAddressDetails.add(lblAddress1);
    vwAddressDetails.add(lblAddress2);
    vwAddressDetails.add(lblAddress3);
    
    vwAddressWrapper.add(vwAddressDetails);
    vwAddressWrapper.add(imgLocate);
    
    vwAddress.add(spacer5);
    vwAddress.add(vwAddressWrapper);
    vwAddress.add(spacer15);
    vwAddress.add(seperator);
    vwAddress.add(spacer5);
    
    
    var a = MAP.api.createAnnotation({
        latitude:   _address.latitude,
        longitude:  _address.longitude,
        title:      _address.Street,
        subtitle:   _address.City+", "+_address.ZIP+" "+_address.State+", "+_address.Country,
        pincolor:   MAP.api.ANNOTATION_RED,
        animate:    true,
        draggable:  false
    });
     
    MAP.mapView.addAnnotation(a);   
    
    vwAddress.addEventListener("click",function(){       
        MAP.setRegion(_address.latitude, _address.longitude, 0.01);        
    });
    
    return vwAddress;
}

function setAddresses(_addresses){
    var setDefaultMap = true;
    for(a in _addresses){
        var vw_address = createAddress( _addresses[a] );
        $.vwListAddress.add(vw_address);
        
        if(setDefaultMap){
            setDefaultMap = true;
            MAP.setRegion(_addresses[a].latitude, _addresses[a].longitude, 0.01);
        }
    }
    
}

function createPhone(_phone){
    var vwPhone = Ti.UI.createView({
        height:Titanium.UI.SIZE,
        layout:"vertical"
    });

    var vwPhoneDetails = Ti.UI.createView({
        height:Titanium.UI.SIZE,
        touchEnable:false,
        bubbleParent:true        
    });
    
    var lblPhone = Ti.UI.createLabel({
        text: _phone,
        width:Titanium.UI.FILL,
        textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT,
        font:{
            fontSize:16,
            fontFamily: "HelveticaNeue-Light"
        },
    });    

    lblPhone.addEventListener("click",function(){
        PHONE.dial(_phone);
    });

    var imgCall = Ti.UI.createImageView({
        image: WPATH("./images/icon_phone.png"),
        right:5,
        width:40,
        height:40
    });

    imgCall.addEventListener("click",function(){
        PHONE.dial(_phone);
    });

    var imgText = Ti.UI.createImageView({
        image: WPATH("./images/icon_sms.png"),
        right:45,
        width:40,
        height:40
    });
    
    imgText.addEventListener("click",function(){
        SMS.compose({
            phones:[_phone]
        });
    });
    
    var seperator   = _createSeperator("#cccccc");
    var spacer     = _createSpacer(10);
    
    vwPhoneDetails.add(lblPhone);
    vwPhoneDetails.add(imgCall);
    vwPhoneDetails.add(imgText);
    
    vwPhone.add(vwPhoneDetails);
    vwPhone.add(spacer);
    vwPhone.add(seperator);
    vwPhone.add(spacer);  
    
    return vwPhone;
}

function setPhones(_phones){
    for(p in _phones){
        var vw_phone = createPhone( _phones[p] );
        $.vwListPhone.add(vw_phone);
    }    
}

function createEmail(_email){
    var vwEmail = Ti.UI.createView({
        height:Titanium.UI.SIZE,
        layout:"vertical"
    });

    var vwEmailDetails = Ti.UI.createView({
        height:Titanium.UI.SIZE,
        touchEnable:false,
        bubbleParent:true        
    });
    
    var lblEmail = Ti.UI.createLabel({
        text: _email,
        width:Titanium.UI.FILL,
        textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT,
        font:{
            fontSize:16,
            fontFamily: "HelveticaNeue-Light"
        },
        touchEnable:false,
        bubbleParent:true        
    });    

    var imgEmail = Ti.UI.createImageView({
        image: WPATH("./images/icon_email.png"),
        right:5,
        width:40,
        height:40,
        touchEnable:false,
        bubbleParent:true        
    });
    

    
    var seperator   = _createSeperator("#cccccc");
    var spacer     = _createSpacer(10);
    
    vwEmailDetails.add(lblEmail);
    vwEmailDetails.add(imgEmail);
    
    vwEmail.add(vwEmailDetails);
    vwEmail.add(spacer);
    vwEmail.add(seperator);
    vwEmail.add(spacer);   

    vwEmail.addEventListener("click",function(){
        EMAIL.compose({
            emails:[_email],
            subject:"",
            message:""
        });
    });
    
    return vwEmail;  
}

function setEmails(_emails){
    for(e in _emails){
        var vw_email = createEmail( _emails[e] );
        $.vwListEmail.add(vw_email);
    }     
}

function _createSpacer(_height){
    var vwSpacer = Ti.UI.createView({
        height:_height,
        touchEnable:false,
        bubbleParent:false
    });
    
    return vwSpacer;
}

function _createSeperator(_color){
    var vwSeperator = Ti.UI.createView({
        backgroundColor:_color,
        height:1,
        touchEnable:false,
        bubbleParent:false
    }); 
    
    return vwSeperator;
}
