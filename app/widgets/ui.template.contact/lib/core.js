// BASIC
// --------------------
// Use this plugin in your app:
//
// `var CORE = require('/plugin/basic/core');`
//
// `CORE.init();`

/* *********************
 * CORE
 * v 1.0
 * ****************** */

var CORE = {

    // `CORE.device.*` return the device info, OS details and battery level/state
    device: {
        simulator: false,
        brand: Titanium.Platform.manufacturer,
        model: Titanium.Platform.model,
        battery: {
            level: Titanium.Platform.batteryLevel,
            state: Titanium.Platform.batteryState,
        },
        os: {
            name: Titanium.Platform.osname,
            version: Titanium.Platform.version,
            major: parseInt(Titanium.Platform.version.split(".")[0], 10),
            minor: parseInt(Titanium.Platform.version.split(".")[1], 10),
        },
        ios_smallscreen: Titanium.Platform.displayCaps.platformHeight == 568 ? false : true,
    },

    // `CORE.app.*` return the app version, default storage, locale
    app: {
        username: Titanium.Platform.username,
        locale: Titanium.Platform.locale,
        version: Titanium.App.version,
        storage: Ti.Filesystem.applicationDataDirectory,
    },

    // `CORE.network.*` return the device network settings and connectivity 
    network: {
        ip: Titanium.Platform.address,
        netmask: Titanium.Platform.netmask,
        mac: Titanium.Platform.macaddress,
        online: Titanium.Network.online,
        type: Titanium.Network.networkType,
    },

    // `CORE.width` and `CORE.height` return the viewport dimensions 
    width: Ti.Platform.displayCaps.platformWidth,
    height: Ti.Platform.displayCaps.platformHeight,
    density: Ti.Platform.displayCaps.dpi,
    densityFactor: Ti.Platform.name == 'android' ? Ti.Platform.displayCaps.logicalDensityFactor : 1,


    /* INIT */

    // Initialise plugin `CORE.init()`
    init: function() {
        if (Titanium.Platform.model == 'google_sdk' || Titanium.Platform.model == 'Simulator') {
            CORE.device.simulator = true;
        } else {
            CORE.device.simulator = false;
        }
        
        if(CORE.densityFactor > 1){
            CORE.width = Math.round(CORE.width / CORE.densityFactor);
            CORE.height = Math.round(CORE.height / CORE.densityFactor);
        }

        CORE.setVersion(CORE.app.version);
    },



    /* DEVICE */

    getVersion: function() {
        return Titanium.App.Properties.getString('app_version');
    },

    setVersion: function(version) {
        CORE.app.version = version;
        Titanium.App.Properties.setString('app_version', version);
    },



    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[CORE] " + string);
                break;

            case "debug":
                Titanium.API.debug("[CORE] " + string);
                break;

            case "error":
                Titanium.API.error("[CORE] " + string);
                break;
        }
    },



    /* OBSERVERS */

    observerOrientation: function(e) {
        CORE.log("info", "core:observerOrientation");
        if(CORE.densityFactor > 1){
            CORE.width = Math.round(Ti.Platform.displayCaps.platformWidth / CORE.densityFactor);
            CORE.height = Math.round(Ti.Platform.displayCaps.platformHeight / CORE.densityFactor);
        }else{
            CORE.width = Ti.Platform.displayCaps.platformWidth;
            CORE.height = Ti.Platform.displayCaps.platformHeight;    
        }
    },

    observerBattery: function(e) {
        CORE.log("info", "core:observerBattery");
        CORE.device.battery.level = e.level;
        CORE.device.battery.state = e.state;
    },

    observerNetwork: function(e) {
        CORE.log("info", "core:observerNetwork");
        CORE.network.online = Titanium.Network.online;
        CORE.network.type = Titanium.Network.networkType;
    }
};

/*****************************/

function eventCoreOrientation(e) {
    CORE.observerOrientation(e);
}

Titanium.Gesture.removeEventListener('orientationchange', eventCoreOrientation);
Titanium.Gesture.addEventListener('orientationchange', eventCoreOrientation);

function eventCoreNetworkChange(e) {
    CORE.observerNetwork(e);
};
Titanium.Network.removeEventListener('change', eventCoreNetworkChange);
Titanium.Network.addEventListener('change', eventCoreNetworkChange);

function eventCoreBattery(e) {
    CORE.observerBattery(e);
};
Titanium.App.removeEventListener('resumed', eventCoreBattery);
Titanium.App.addEventListener('resumed', eventCoreBattery);


/*****************************/

module.exports = CORE;