// BASIC
// --------------------
// Use this plugin in your app:
//
// `var CONTACT = require('/plugin/basic/contact');`
//
// `CONTACT.init();`

/* *********************
 * CONTACT
 * v 1.0
 * *********************
 *
 *
 *  (!) ANDROID PERMISSION IN TIAPP.XML
 *      <uses-permission android:name="android.permission.READ_CONTACTS"/>
 *      <uses-permission android:name="android.permission.WRITE_CONTACTS"/>
 *
 */
var CONTACT = {

    // `CONTACT.api.*` links to the Titanium Contacts API 
    api: Titanium.Contacts,


    /* INIT */

    // Initialise plugin and request access to the Address Book `CONTACT.init()`
    init: function() {
        if (OS_IOS) {
            var access = CONTACT.getAccess();
            if (access == 0 || access == null) {
                CONTACT.requestAccess();
            }
        }

        if (OS_ANDROID) {
            CONTACT.setAccess(1);
        }
    },

    // Check if the app is authorized to access the Address Book `CONTACT.getAccess()`
    //
    // `@output: true|false`
    getAccess: function() {
        return Number(Titanium.App.Properties.getString('contact_access'));
    },

    setAccess: function(contact_access) {
        Titanium.App.Properties.setString('contact_access', contact_access);
    },

    requestAccess: function() {
        if (CONTACT.api.contactsAuthorization == CONTACT.api.AUTHORIZATION_AUTHORIZED) {
            CONTACT.setAccess(1);
        } else if (CONTACT.api.contactsAuthorization == CONTACT.api.AUTHORIZATION_UNKNOWN) {
            CONTACT.api.requestAuthorization(function(e) {
                if (e.success) {
                    CONTACT.setAccess(1);
                } else {
                    CONTACT.setAccess(0);
                }
            });
        } else {
            CONTACT.setAccess(0);
        }
    },

    /* GLOBAL */

    // `CONTACT.getAddressBook()` return a filtered collection of persons
    //
    // `@output: [{id, first_name, last_name, emails[], phones[], fb_username}]`
    getAddressBook: function() {
        var addressbook = Titanium.Contacts.getAllPeople();
        var light_ab = CONTACT.getAddressBookFiltered(addressbook);
        return light_ab;
    },

    getAddressBookFiltered: function(addressbook) {
        var list = Array();
        for (c = 0; c < addressbook.length; c++) {

            if (OS_IOS) {
                var cid = addressbook[c].recordId;
            } else {
                var cid = addressbook[c].id;
            }

            var firstname = "";
            var lastname = "";
            var emails = CONTACT.getEmail(addressbook[c].email);
            var phones = CONTACT.getPhone(addressbook[c].phone);
            var fbusername = null;

            addressbook[c].firstName != null ? firstname = addressbook[c].firstName : "";
            addressbook[c].lastName != null ? lastname = addressbook[c].lastName : "";


            if (typeof addressbook[c].instantMessage != "undefined" && addressbook[c].instantMessage != null) {
                var social_username = addressbook[c].instantMessage;
                if (typeof social_username.instantMessage != "undefined" && social_username.instantMessage != null) {
                    for (im = 0; im < social_username.instantMessage.length; im++) {
                        if (social_username.instantMessage[im].service.toLowerCase() == 'facebook') {
                            fbusername = social_username.instantMessage[im].username;
                        }
                    }
                }
            }

            var p = {
                id: cid,
                first_name: firstname,
                last_name: lastname,
                emails: emails,
                phones: phones,
                fb: fbusername
            };

            list.push(p);
        }

        return list;
    },

    // `CONTACT.pick()` open the contact picker
    //
    // `@input: param (Object)`
    //
    // `@callback: param.selectedPerson, param.cancel`
    pick: function(param) {
        CONTACT.api.showContacts(param);
    },


    /* PERSON */

    // `CONTACT.getAllPerson()` return all contacts from the Address Book
    //
    // `@output: [{Titanium.Contacts.Person}]`
    getAllPerson: function() {
        var addressbook = CONTACT.api.getAllPeople();
        return addressbook;
    },

    // `CONTACT.getPerson(person_id)` return a contact by ID
    //
    // `@input: person_id (int)`
    //
    // `@ouput: {Titanium.Contacts.Person}`
    getPerson: function(person_id) {
        CONTACT.log("info", "getPerson > " + JSON.stringify(person_id));
        var person = CONTACT.api.getPersonByID(person_id);
        return person;
    },

    // `CONTACT.addPerson(param)` add a contact in the Address Book
    //
    // `@input: param {Titanium.Contacts.Person}`
    //
    // `@output: {Titanium.Contacts.Person}`
    //
    // `@callback: param.callback (optional)`
    addPerson: function(param) {
        CONTACT.log("info", "addPerson > " + JSON.stringify(param.person));
        var person = CONTACT.api.createPerson(param.person);
        if (typeof param.callback != "undefined") {
            param.callback(person);
        } else {
            return person;
        }
    },

    // `CONTACT.removePerson(param)` remove a contact from the Address Book
    //
    // `@input: param {Titanium.Contacts.Person}`
    //
    // `@callback: param.callback (optional)`
    removePerson: function(param) {
        CONTACT.log("info", "removePerson > " + JSON.stringify(param.person));
        CONTACT.api.removePerson(param.person);
        if (OS_IOS) {
            CONTACT.api.save();
        }
        if (typeof param.callback != "undefined") {
            param.callback();
        }
    },


    /* GROUP */

    // `CONTACT.getAllGroup()` return all groups from the Address Book
    //
    // `@output: [{Titanium.Contacts.Group}]`
    getAllGroup: function() {
        if (OS_IOS) {
            var list = CONTACT.api.getAllGroups();
            return list;
        } else {
            CONTACT.log('error', 'getAllGroup > Only supported by iOS');
            return false;
        }
    },

    // `CONTACT.getGroup(group_id)` return a group by ID
    //
    // `input: group_id (int)`
    //
    // `output: {Titanium.Contacts.Group}`    
    getGroup: function(group_id) {
        if (OS_IOS) {
            CONTACT.log("info", "getGroup > " + JSON.stringify(group_id));
            var group = CONTACT.api.getGroupByID(group_id);
            return group;
        } else {
            CONTACT.log('error', 'getGroup > Only supported by iOS');
            return false;
        }
    },

    // `CONTACT.getGroupMembers(group_id)` return a collection of contacts attached to a group
    //
    // `@input: group_id (int)`
    //
    // `@output: [{Titanium.Contacts.Person}]` 
    getGroupMembers: function(group_id) {
        if (OS_IOS) {
            CONTACT.log("info", "getGroupMembers > " + JSON.stringify(group_id));
            var list = CONTACT.api.getGroupByID(group_id).members();
            return list;
        } else {
            CONTACT.log('error', 'getGroupMembers > Only supported by iOS');
            return false;
        }
    },

    // `CONTACT.addGroup(param)` add a group in the Address Book
    //
    // `@input: param {Titanium.Contacts.Group}`
    //
    // `@output: {Titanium.Contacts.Group}`
    //
    // `@callback: param.callback (optional)`
    addGroup: function(param) {
        if (OS_IOS) {
            CONTACT.log("info", "addGroup > " + JSON.stringify(param.group));
            var group = CONTACT.api.createGroup(param.group);
            CONTACT.api.save();
            if (typeof param.callback != "undefined") {
                param.callback(group);
            } else {
                return group;
            }
        } else {
            CONTACT.log('error', 'addGroup > Only supported by iOS');
            return false;
        }
    },

    // `CONTACT.removeGroup(param)` remove a group in the Address Book
    //
    // `@input: param {Titanium.Contacts.Group}`
    //
    // `@callback: param.callback (optional)`
    removeGroup: function(param) {
        if (OS_IOS) {
            CONTACT.log("info", "removeGroup > " + JSON.stringify(param.group));
            CONTACT.api.removeGroup(param.group);
            CONTACT.api.save();
            if (typeof param.callback != "undefined") {
                param.callback();
            }
        } else {
            CONTACT.log('error', 'removeGroup > Only supported by iOS');
            return false;
        }
    },


    /* UTILS */

    getAllEmails: function(list) {
        var result = Array();
        for (var key in list) {
            var obj = list[key];
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {

                    var validateEmail = /([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/g;
                    if (validateEmail.test(obj[prop]) && obj[prop] !== '') {
                        result.push(obj[prop]);
                    }

                }
            }
        }
        CONTACT.log("info", "getAllEmails > " + JSON.stringify(result));
        return result;
    },

    getAllPhones: function(list) {
        var result = Array();
        for (var key in list) {
            var obj = list[key];
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    result.push(obj[prop]);
                }
            }
        }
        CONTACT.log("info", "getAllPhones > " + JSON.stringify(result));
        return result;
    },


    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[CONTACT] " + string);
                break;

            case "debug":
                Titanium.API.debug("[CONTACT] " + string);
                break;

            case "error":
                Titanium.API.error("[CONTACT] " + string);
                break;
        }
    }


};

/*****************************/

module.exports = CONTACT;