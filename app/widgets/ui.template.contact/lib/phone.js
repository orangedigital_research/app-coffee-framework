// NETWORK
// --------------------
// Use this plugin in your app:
//
// `var PHONE = require('/plugin/network/phone');`
//
// `PHONE.init();`
//
// `PHONE.dial(phone);`

/* *********************
 * PHONE
 * v 1.0
 * *********************
 */
var PHONE = {

    enabled: false,

    /* INIT */

    init: function() {
        if (OS_IOS) {
            PHONE.enabled = Titanium.Platform.canOpenURL("tel:");
        }

        if (OS_ANDROID) {
            PHONE.enabled = Titanium.Platform.openURL("tel:");
        }
    },


    /* ACTION */

    // `PHONE.dial(phone)` dial a phone number
    //
    // `@input: phone (string)`
    dial: function(phone) {
        PHONE.init();

        if (PHONE.enabled) {
            PHONE.log('info', 'dial phone > ' + JSON.stringify(phone));
            Titanium.Platform.openURL('tel:' + phone.replace(/ /g, ''));
        } else {
            var nophone_alt = Ti.UI.createAlertDialog({
                message: 'Your device has no voice call capabilities',
                ok: 'OK',
                title: 'Alert'
            }).show();
            nophone_alt = null;
        }
    },


    // `PHONE.pick(param)` pick a phone number from a list and compose a new email
    //
    // `@input: param (Object)`
    //
    // `@callback: param.cancel (function)`
    pick: function(param) {
        if (param.phones.length == 0) {
            PHONE.alertNoPhone();
        } else if (param.phones.length == 1) {
            /* Only 1 phone number */
            PHONE.dial(param.phones[0]);
        } else {
            /* Multiple phone numbers */
            var phone_choice = param.phones;
            phone_choice.push('Cancel');
            var cancel_pos = phone_choice.length - 1;
            var optionPhones = {
                options: phone_choice,
                cancel: cancel_pos
            };
            var optionPhoneDialog = Titanium.UI.createOptionDialog(optionPhones);
            optionPhoneDialog.addEventListener('click', function(e) {
                switch (e.index) {
                    default:
                    /* Call */
                    var phone = phone_choice[e.index];
                    PHONE.dial(phone);
                    break;

                    case cancel_pos:
                        /* Cancel */
                        if (typeof param.cancel != "undefined") {
                            param.cancel();
                        }
                        break;
                }
            });

            optionPhoneDialog.show();

        }

    },

    /* ALERT */

    alertNoPhone: function() {
        var network_alt = Ti.UI.createAlertDialog({
            message: 'No phone number provided',
            ok: 'OK',
            title: 'Notice'
        }).show();
        network_alt = null;
    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[PHONE] " + string);
                break;

            case "debug":
                Titanium.API.debug("[PHONE] " + string);
                break;

            case "error":
                Titanium.API.error("[PHONE] " + string);
                break;
        }
    },


};

/*****************************/

module.exports = PHONE;