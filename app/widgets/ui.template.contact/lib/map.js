// LOCATION
// --------------------
// Use this plugin in your app:
//
// `var MAP = require('/plugin/location/map');`
//
// `MAP.init($.modMap);`

/* *********************
 * MAP
 * v 1.0
 * *********************
 */
var MAP = {

    // `MAP.api.*` links to the Titanium Map API 
    api: require('ti.map'),

    mapView: null,


    /* INIT */

    // Initialise plugin and set the mapView `MAP.init(mapView)`
    //
    // `@input: mapView (Map Module View)`
    init: function(mapView) {
        MAP.mapView = mapView;
    },


    /* ACTION */

    // `MAP.setRegion(lat, lng, delta)` move and zoom the map to a specific location
    //
    // `@input: lat (int)`
    //
    // `@input: lng (int)`
    //
    // `@input: delta (int)`
    setRegion: function(lat, lng, delta) {
        var region = {
            latitude: lat,
            longitude: lng,
            latitudeDelta: delta,
            longitudeDelta: delta
        };
        MAP.mapView.setRegion(region);
    },

    // `MAP.convertPixeltoLatLng(x,y)` convert X,Y to Lat,Lng
    //
    // `@input: x (int)`
    //
    // `@input: y (int)`
    //
    // `@output: {latitude, longtitude}`
    convertPixeltoLatLng: function(x, y) {
        var region = MAP.mapView.actualRegion || MAP.mapView.region;
        var widthInPixels = MAP.mapView.rect.width;
        var heightInPixels = MAP.mapView.rect.height;

        var heightDegPerPixel = -region.latitudeDelta / heightInPixels;
        var widthDegPerPixel = region.longitudeDelta / widthInPixels;

        coordinates = {
            latitude: (y - heightInPixels / 2) * heightDegPerPixel + region.latitude,
            longitude: (x - widthInPixels / 2) * widthDegPerPixel + region.longitude
        };

        return coordinates;
    },

    // `MAP.pickType(x,y)` open a option dialog and let the user set the map terrain mode
    pickType: function() {
        var optionMapTypeChoice = {
            options: ['Normal', 'Satellite', 'Hybrid', 'Cancel'],
            cancel: 3,
        };
        var optionMapType = Titanium.UI.createOptionDialog(optionMapTypeChoice);
        optionMapType.addEventListener('click', function(e) {
            switch (e.index) {
                case 0:
                    MAP.switchType('normal');
                    break;

                case 1:
                    MAP.switchType('satellite');
                    break;

                case 2:
                    MAP.switchType('hybrid');
                    break;

                case 3: //Cancel
                    break;
            }
        });

        optionMapType.show();
    },

    switchType: function(t) {
        switch (t) {
            case "normal":
                MAP.mapView.setMapType(MAP.api.NORMAL_TYPE);
                break;

            case "satellite":
                MAP.mapView.setMapType(MAP.api.SATELLITE_TYPE);
                break;

            case "hybrid":
                MAP.mapView.setMapType(MAP.api.HYBRID_TYPE);
                break;

            case "terrain":
                MAP.mapView.setMapType(MAP.api.TERRAIN_TYPE);
                break;
        }
    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[MAP] " + string);
                break;

            case "debug":
                Titanium.API.debug("[MAP] " + string);
                break;

            case "error":
                Titanium.API.error("[MAP] " + string);
                break;
        }
    }

};

/*****************************/

module.exports = MAP;