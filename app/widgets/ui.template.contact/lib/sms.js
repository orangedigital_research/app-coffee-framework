// NETWORK
// --------------------
// Use this plugin in your app:
//
// `var SMS = require('/plugin/network/sms');`
//
// `SMS.init();`
//
// `SMS.compose(param);`

/* *********************
 * SMS
 * v 1.0
 * *********************
 */
var SMS = {

    api: null,
    enabled: false,


    /* INIT */

    init: function() {
        if (OS_IOS) {
            SMS.api = require("com.omorandi");
            SMS.enabled = Titanium.Platform.canOpenURL("tel:");
        }

        if (OS_ANDROID) {
            SMS.api = null;
            SMS.enabled = Titanium.Platform.openURL("tel:");
        }

    },

    /* ACTIONS */

    // `SMS.compose(phone)` compose a new text message
    //
    // `@input: phone (array)`
    //
    // `@input: param.message (string)`
    compose: function(param) {
        SMS.init();
        if (SMS.enabled) {
            SMS.log('info', 'compose > ' + JSON.stringify(param.phones));
            if (OS_IOS) {

                var smsDialog = SMS.api.createSMSDialog({
                    recipients: param.phones,
                    messageBody: param.message,
                });
                smsDialog.open({
                    animated: true
                });

            } else {

                var intent = Ti.Android.createIntent({
                    action: Ti.Android.ACTION_SENDTO,
                    data: 'smsto:' + param.phones.join(',')
                });
                intent.putExtra('sms_body', param.message);
                Ti.Android.currentActivity.startActivity(intent);

            }
        } else {
            var nophone_alt = Ti.UI.createAlertDialog({
                message: 'Your device has no SMS messaging capabilities',
                ok: 'OK',
                title: 'Alert'
            }).show();
            nophone_alt = null;
        }
    },


    // `SMS.pick(param)` pick an phone number from a list and compose a new text message
    //
    // `@input: param (Object)`
    //
    // `@callback: param.cancel (function)`
    pick: function(param) {
        if (param.phones.length == 0) {
            SMS.alertNoPhone();
        } else if (param.phones.length == 1) {
            /* Only 1 phone number */
            SMS.compose(param);
        } else {
            /* Multiple phone numbers */
            var phone_choice = param.phones;
            phone_choice.push('Cancel');
            var cancel_pos = phone_choice.length - 1;
            var optionPhones = {
                options: phone_choice,
                cancel: cancel_pos
            };
            var optionPhoneDialog = Titanium.UI.createOptionDialog(optionPhones);
            optionPhoneDialog.addEventListener('click', function(e) {
                switch (e.index) {
                    default:
                    /* Compose */
                    param.phones = [phone_choice[e.index]];
                    SMS.compose(param);
                    break;

                    case cancel_pos:
                        /* Cancel */
                        if (typeof param.cancel != "undefined") {
                            param.cancel();
                        }
                        break;
                }
            });

            optionPhoneDialog.show();

        }

    },

    /* ALERT */

    alertNoPhone: function() {
        var network_alt = Ti.UI.createAlertDialog({
            message: 'No phone number provided',
            ok: 'OK',
            title: 'Notice'
        }).show();
        network_alt = null;
    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[SMS] " + string);
                break;

            case "debug":
                Titanium.API.debug("[SMS] " + string);
                break;

            case "error":
                Titanium.API.error("[SMS] " + string);
                break;
        }
    },


};

/*****************************/

module.exports = SMS;