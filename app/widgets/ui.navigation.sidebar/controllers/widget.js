var CORE        = require(WPATH("core"));
CORE.init();

$.init = function(_param){ 
    $.Wrapper.backgroundColor   = _param.backgroundColor;
    $.state                     = false;

    if( typeof _param.backgroundGradient != 'undefined' &&  _param.backgroundGradient != null){
        $.Wrapper.backgroundGradient = _param.backgroundGradient;
    }
    
    if(OS_IOS && CORE.device.os.major >= 7){
        var _padding = 20;
    }else{
        var _padding = 0;
    }
    
    if(_param.zindex == "under"){
        switch(_param.position){
            case "left":  
                $.items.left             = 0;                
                if(_param.sfx == "scale"){
                    $.items.height       = Ti.UI.SIZE;
                }else{
                    $.items.width        = _param.width;
                    $.items.top          = _padding;
                }
            break;
            
            case "right":
                $.items.right            = 0;
                if(_param.sfx == "scale"){
                    $.items.width        = _param.width;
                    $.items.height       = Ti.UI.SIZE;
                }else{
                    $.items.width        = _param.width;
                    $.items.top          = _padding;
                }
            break;
            
            case "top":
                _param.defaultParam.width = CORE.width / _param.items.length; 
                            
                $.Wrapper.top           = _padding;
                $.Wrapper.height        = _param.height;      
            break;
            
            case "bottom":
                _param.defaultParam.width = CORE.width / _param.items.length;
                    
                $.Wrapper.bottom        = 0;
                $.Wrapper.height        = _param.height;                    
            break;
        }
    }else{
        switch(_param.position){
            case "left": 
                $.Wrapper.top           = 50 + _padding;
                $.Wrapper.left          = _param.width * -1;
                $.Wrapper.width         = _param.width;
            break;
            
            case "right":
                $.Wrapper.top           = 50 + _padding;
                $.Wrapper.right         = _param.width * -1;
                $.Wrapper.width         = _param.width;
            break;
            
            case "top":
                _param.defaultParam.width = CORE.width / _param.items.length; 
                            
                $.Wrapper.top           = _param.height * -1;
                $.Wrapper.height        = _param.height;      
            break;
            
            case "bottom":
                _param.defaultParam.width = CORE.width / _param.items.length;
                    
                $.Wrapper.bottom        = _param.height * -1;
                $.Wrapper.height        = _param.height;                    
            break;
        }        
    }
    
    for(i in _param.items){
        var item = create(_param.items[i], _param.defaultParam, _param.callback);       
        $.items.add(item);
    }
    
    
    var animations = getMenuAnimations(_param.position, _param.zindex, _param.sfx,  _param.width, _param.height);         
    
    $.toggle = function(){
        if($.state){
           $.state = false;
           _param.mainstage.touchEnabled = true;
           if(_param.zindex == "under"){              
               _param.mainstage.animate( animations.close );
           }else{
               $.Wrapper.animate( animations.close );
           }
           
        }else{
           $.state = true; 
           _param.mainstage.touchEnabled = false;
           if(_param.zindex == "under"){             
               _param.mainstage.animate( animations.open );
           }else{
               $.Wrapper.animate( animations.open );
           }
        } 
        
        return $.state;    
    };
    
};

$.select = function(_id){
    select(_id);
};

function getMenuAnimations(position, zindex, sfx, width, height){
    if(zindex == "under"){
        switch(position){
            case "left":
                if(sfx == "scale"){
                    var scale_open  = Ti.UI.create2DMatrix();
                    scale_open      = scale_open.scale(0.8,0.8);
                    
                    var scale_close = Ti.UI.create2DMatrix();
                    scale_close     = scale_close.scale(1,1);
                    
                    var animation_open      = Titanium.UI.createAnimation({left: width * 0.9, transform: scale_open, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                    var animation_close     = Titanium.UI.createAnimation({left:0, transform: scale_close, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});           
                }else{
                    var animation_open      = Titanium.UI.createAnimation({left: width, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                    var animation_close     = Titanium.UI.createAnimation({left:0, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});           
                }
            break;
            
            case "right":
                if(sfx == "scale"){
                    var scale_open  = Ti.UI.create2DMatrix();
                    scale_open      = scale_open.scale(0.8,0.8);
                    
                    var scale_close = Ti.UI.create2DMatrix();
                    scale_close     = scale_close.scale(1,1);                   
                    
                    var animation_open      = Titanium.UI.createAnimation({right: width * 0.9, transform: scale_open, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                    var animation_close     = Titanium.UI.createAnimation({right:0, transform: scale_close, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
                }else{
                    var animation_open      = Titanium.UI.createAnimation({right: width, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                    var animation_close     = Titanium.UI.createAnimation({right:0, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
                }
            break;
            
            case "top":
                var animation_open      = Titanium.UI.createAnimation({top:height, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                var animation_close     = Titanium.UI.createAnimation({top:0, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
            break;
            
            case "bottom":
                var animation_open      = Titanium.UI.createAnimation({bottom:height, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                var animation_close     = Titanium.UI.createAnimation({bottom:0, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
            break;
        }
    }else{
       switch(position){
            case "left":  
                var animation_open      = Titanium.UI.createAnimation({left: 0, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                var animation_close     = Titanium.UI.createAnimation({left: width * -1, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
            break;
            
            case "right":
                var animation_open      = Titanium.UI.createAnimation({right: 0, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                var animation_close     = Titanium.UI.createAnimation({right: width * -1, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
            break;
            
            case "top":
                if(OS_IOS && CORE.device.os.major >= 7){
                    var top_padding = 20;
                }else{
                    var top_padding = 0;
                }
                       
                var animation_open      = Titanium.UI.createAnimation({top:top_padding, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                var animation_close     = Titanium.UI.createAnimation({top: height * -1, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
            break;
            
            case "bottom":
                var animation_open      = Titanium.UI.createAnimation({bottom:0, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_OUT});
                var animation_close     = Titanium.UI.createAnimation({bottom: height * -1, duration:200, curve: Ti.UI.ANIMATION_CURVE_EASE_IN});
            break;
        }        
    }

    var animations = {
        open: animation_open,
        close: animation_close
    };
    
    return animations;
}

function create(_param, _default, _callback){
    
    var itemView = Titanium.UI.createView({
       id:          _param.id,
       height:      45,
       left:        _default.left,
       right:       _default.right,
       selected:    _param.selected
    });
    
    var itemSeparator = Titanium.UI.createView({
       id:       "itemSeparator_"+_param.id,
       height:   1,
       bottom:   0,
       touchEnabled:   false,
       bubbleParent:   false,
       backgroundColor: _default.seperatorColor
    });
  
    
    var icn = imageSelectedMethod(_param.icon,_param.iconSelectedMethod);
    
    var itemIcon = Titanium.UI.createImageView({
        id:             "itemIcon_"+_param.id,
        left:           10,
        touchEnabled:   false,
        bubbleParent:   false,
        image:          icn.src,
        width:          30,
        height:         30,
        opacity:        icn.opacityDeselected,        
    });
    
    if(_param.title != null){
       
        var itemLabel = Titanium.UI.createLabel({
            id:             "itemLabel_"+_param.id,
            left:           50,
            width:          Ti.UI.SIZE,
            height:         Ti.UI.SIZE,
            touchEnabled:   false,
            bubbleParent:   false,
            color:          _default.fontColor,
            font: {
                fontSize: _default.fontSize,
                fontFamily: _default.fontFamily,
            },
            textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
            text: _param.title,
        });  
        
        itemView.add(itemIcon);
        itemView.add(itemLabel);      
    }else{
        itemView.add(itemIcon);
    }
    
    itemView.add(itemSeparator);

    
    itemView.select = function(){
        var icn = imageSelectedMethod(_param.icon,_param.iconSelectedMethod);
        
        itemView.backgroundColor = _default.backgroundColorSelected;
        itemView.selected        = true;
        
        itemIcon.image           = icn.src;
        itemIcon.opacity         = icn.opacitySelected;
        
        if(_param.title != null){
            itemLabel.color      = _default.fontColorSelected;
        }
    };
    
    itemView.deselect = function(){
        var icn = imageSelectedMethod(_param.icon,_param.iconSelectedMethod);
        
        itemView.backgroundColor = _default.backgroundColor;
        itemView.selected        = false;
        
        itemIcon.image           = icn.src;
        itemIcon.opacity         = icn.opacityDeselected;
        
        if(_param.title != null){
            itemLabel.color      = _default.fontColor;
        }
    };
    
    itemView.addEventListener("click",function(){
        _callback(_param);        
        select(itemView.id);
    });
    
    if(_param.selected){
        itemView.select();
    }
    
    return itemView;
}

function select(_id){
    //deselectAll();
    
    var items = $.items.getChildren();
    for(i in items){
        if(items[i].id == _id){
           items[i].select();
           //items[i].touchEnabled = false;
           //items[i].bubbleParent = false;
        }else{
           items[i].deselect();
           //items[i].touchEnabled = true;
           //items[i].bubbleParent = true;
        }     
    }
}

function deselectAll(){
    var items = $.items.getChildren();
    for(i in items){
        items[i].deselect();
        //items[i].touchEnabled = true;
        //items[i].bubbleParent = true;
    }
}

function imageSelectedMethod(_image,_method){
    switch(_method){
        default:
        case "opacity":
            var imageSelectedParam = {
                src: _image,
                opacitySelected: 1,
                opacityDeselected: 0.5,
            };
        break;
        
        case "file":
            var icn = _image.split(".");
            var imageSelectedParam = {
                src: icn[0]+"_selected."+icn[1],
                opacitySelected: 1,
                opacityDeselected: 1,
            };
        break;
    }
    return imageSelectedParam;
}
