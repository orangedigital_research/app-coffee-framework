var CORE = require(WPATH("core"));

$.init = function(_param) {

    $.setTitle(_param.title, _param.fontColor, _param.fontFamily, _param.fontSize);
    $.setTintColor(_param.buttonTint);

    $.Wrapper.backgroundColor = _param.backgroundColor;

    if( typeof _param.backgroundGradient != 'undefined' &&  _param.backgroundGradient != null){
        $.Wrapper.backgroundGradient = _param.backgroundGradient;
    }

    if (OS_IOS && CORE.device.os.major >= 7) {
        $.Wrapper.height = 70;
        $.overlay.top = 20;
    }

};

$.setTintColor = function(_color) {
    $.leftText.color = _color;
    $.rightText.color = _color;
    $.leftText.buttonTint = _color;
    $.rightText.buttonTint = _color;
};

$.setTitle = function(title, fontColor, fontFamily, fontSize) {
    $.title.color = fontColor;
    $.title.text = title;
    $.title.font = {
        fontSize : fontSize,
        fontFamily : fontFamily
    };
};

$.setTitleText = function(title) {
    $.title.text = title;
};

$.setLeftTitle = function(title){
    $.leftText.text = title;
};

$.setRightTitle = function(title){
    $.rightText.text = title;
};

$.setLeftImage = function(image){
    $.leftImage.image = image;
};

$.setRightImage = function(image){
    $.rightImage.image = image;
};

$.setLeft = function(_params) {
    
    if(_params.visible){
        $.showLeft();
    }
    
    if (_params.text != null) {
        $.leftText.text = _params.text;
        $.leftImage.image = null;
    } else {
        $.leftText.text = null;
        $.leftImage.image = _params.image;
    }

    $.left.addEventListener("click", _params.callback);
};
$.showLeft = function() {
    $.left.show();
    //$.right.removeEventListener("click", $.right_callback);
};
$.hideLeft = function() {
    $.left.hide();
    //$.left.removeEventListener("click", $.left_callback);
};

$.setRight = function(_params) {
    
    if(_params.visible){
        $.showRight();
    }
    
    if (_params.text != null) {
        $.rightText.text = _params.text;
        $.rightImage.image = null;
    } else {
        $.rightText.text = null;
        $.rightImage.image = _params.image;
    }

    $.right.addEventListener("click", _params.callback);
};
$.showRight = function() {
    $.right.show();
    //$.right.removeEventListener("click", $.right_callback);
};
$.hideRight = function() {
    $.right.hide();
    //$.right.removeEventListener("click", $.right_callback);
};

$.setMenu = function(_callback) {
    $.setLeft({
        visible:true,
        text : null,
        image : WPATH("/images/icon_menu.png"),
        callback : _callback
    });
};

$.setBack = function(_callback) {
    $.setLeft({
        visible:true,
        text : null,
        image : WPATH("/images/icon_back.png"),
        callback : _callback
    });
};

$.setClose = function(_callback) {
    $.setLeft({
        visible:true,
        text : null,
        image : WPATH("/images/icon_close.png"),
        callback : _callback
    });
};

$.setSettings = function(_callback) {
    $.setRight({
        visible:true,
        text : null,
        image : WPATH("/images/icon_settings.png"),
        callback : _callback
    });
};

// Action

var observerLeftRollOver = function() {
    $.left.opacity = 0.5;
};
var observerLeftRollOut = function() {
    $.left.opacity = 1;
};
$.left.addEventListener("touchstart", observerLeftRollOver);
$.left.addEventListener("touchend", observerLeftRollOut);
$.left.addEventListener("touchmove", observerLeftRollOut);

var observerRightRollOver = function() {
    $.right.opacity = 0.5;
};
var observerRightRollOut = function() {
    $.right.opacity = 1;
};
$.right.addEventListener("touchstart", observerRightRollOver);
$.right.addEventListener("touchend", observerRightRollOut);
$.right.addEventListener("touchmove", observerRightRollOut);
