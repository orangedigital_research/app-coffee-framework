var CORE        = require(WPATH("core"));

$.init = function(_param){    
    CORE.init();
    if(_param.extend){
        $.Wrapper.width             = CORE.width;
        $.Wrapper.left              = _param.padding;
        $.Wrapper.right             = _param.padding; 
        _param.defaultParam.width   = (CORE.width - (_param.padding * 2) ) / _param.items.length;     
    }else{
        $.Wrapper.width             = Ti.UI.SIZE;
        $.guideline.width           = 50 * (_param.items.length - 1);
        _param.defaultParam.width   = 50; 
    }
    
    $.guideline.backgroundColor = _param.defaultParam.primaryColor;
    
    
    for(t in _param.items){
        var item = create(_param.items[t], _param.defaultParam, _param.callback);       
        $.items.add(item);
    }
   
    
};


$.select = function(_id){
    select(_id);
};


function create(_param, _default, _callback){
    
    var itemView = Titanium.UI.createView({
       id:          _param.id,
       width:       _default.width,
       selected:    _param.selected
    });  
    
    
   var itemBullet = Titanium.UI.createView({
        id:             "itemBullet_"+_param.id,
        width:          _default.bulletSize,
        height:         _default.bulletSize,
        borderWidth:    _default.bulletWidth,
        borderRadius:   _default.bulletRadius,
        borderColor:    _default.primaryColor,
        backgroundColor:_default.bulletColor,
        touchEnabled:   false,
        bubbleParent:   false,        
    });
    
    var itemBulletSelected = Titanium.UI.createView({
        id:             "itemBulletSelected_"+_param.id,
        width:          _default.bulletSize  * 0.5,
        height:         _default.bulletSize  * 0.5,
        borderWidth:    _default.bulletWidth * 0.5,
        borderRadius:   _default.bulletRadius * 0.5,
        backgroundColor:_default.bulletColorSelected,
        opacity:        0,
        touchEnabled:   false,
        bubbleParent:   false,        
    });
    itemBullet.add(itemBulletSelected);
    
    
    if(_param.title != null){
        
        var itemLabel = Titanium.UI.createLabel({
            id: "itemLabel_"+_param.id,
            width:Ti.UI.FILL,
            opacity:0,
            touchEnabled:false,
            bubbleParent:false,
            color: _default.fontColor,
            font: {
                fontSize: 12,
                fontFamily: _default.fontFamily,
            },
            textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
            text: _param.title,
        });  
        
        if(_default.labelPosition == "top"){
            itemLabel.top       = 0;
        }else{
            itemLabel.bottom    = 0;
        }
        
        itemView.add(itemBullet);
        itemView.add(itemLabel);      
    }else{
        itemView.add(itemBullet);
    }


    
    itemView.select = function(){        
        itemView.selected           = true;        
        itemBulletSelected.opacity  = 1;
             
        if(_param.title != null){
            itemLabel.opacity       = 1;
        }
    };
    
    itemView.deselect = function(){
        itemView.selected           = false;        
        itemBulletSelected.opacity  = 0;
        
        if(_param.title != null){
            itemLabel.opacity       = 0;
        }
    };

    if(_default.clickable){
        itemView.addEventListener("click",function(){
            _callback(_param);        
            deselectAll();
            itemView.select();
        });        
    }

    
    if(_param.selected){
        itemView.select();
    }
    
    return itemView;
}

function select(_id){
    deselectAll();
    
    var items = $.items.getChildren();
    for(i in items){
        if(items[i].id == _id && items[i].selected == false){
           items[i].select(); 
        }      
    }
}

function deselectAll(){
    var items = $.items.getChildren();
    for(i in items){
        items[i].deselect();
    }
}