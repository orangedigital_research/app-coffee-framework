var CORE        = require(WPATH("core"));

$.init = function(_param){    
    $.Wrapper.backgroundColor = _param.backgroundColor;
    
    if( typeof _param.backgroundGradient != 'undefined' &&  _param.backgroundGradient != null){
        $.Wrapper.backgroundGradient = _param.backgroundGradient;
    }
    
    _param.defaultParam.width = 100 / _param.items.length;

    for(i in _param.items){
        var item = create(_param.items[i], _param.defaultParam, _param.callback);       
        $.items.add(item);
    }
   
    
};


$.select = function(_id){
    select(_id);
};


function create(_param, _default, _callback){
    
    var itemView = Titanium.UI.createView({
       id:       _param.id,
       width:    _default.width+"%",
       selected: _param.selected
    });  
    
    var icn = imageSelectedMethod(_param.icon,_param.iconSelectedMethod);
    
    var itemIcon = Titanium.UI.createImageView({
        id:             "itemIcon_"+_param.id,
        touchEnabled:   false,
        bubbleParent:   false,
        image:          icn.src,
        opacity:        icn.opacityDeselected,
        width:          30,
        height:         30       
    });
    
    if(_param.title != null){
        itemIcon.top = 2;
        
        var itemLabel = Titanium.UI.createLabel({
            id: "itemLabel_"+_param.id,
            width:Ti.UI.FILL,
            bottom:2,
            touchEnabled:false,
            bubbleParent:false,
            color: _default.fontColor,
            font: {
                fontSize: 12,
                fontFamily: _default.fontFamily,
            },
            textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
            text: _param.title,
        });  
        
        itemView.add(itemIcon);
        itemView.add(itemLabel);      
    }else{
        itemView.add(itemIcon);
    }


    
    itemView.select = function(){
            
        var icn = imageSelectedMethod(_param.icon,_param.iconSelectedMethod);
        
        itemView.backgroundColor = _default.backgroundColorSelected;
        itemView.selected        = true;
        
        itemIcon.image           = icn.src;
        itemIcon.opacity         = icn.opacitySelected;
        
        if(_param.title != null){
            itemLabel.color      = _default.fontColorSelected;
        }            

    };
    
    itemView.deselect = function(){

        var icn = imageSelectedMethod(_param.icon,_param.iconSelectedMethod);
    
        itemView.backgroundColor = "transparent";
        itemView.selected        = false;
        
        itemIcon.image           = icn.src;
        itemIcon.opacity         = icn.opacityDeselected;
        
        if(_param.title != null){
            itemLabel.color      = _default.fontColor;
        }


    };
    
    itemView.addEventListener("click",function(){
        _callback(_param);        
        select(itemView.id);
    });
    
    if(_param.selected){
        itemView.select();
    }
    
    return itemView;
}

function select(_id){
    //deselectAll();
    
    var items = $.items.getChildren();
    for(i in items){
        if(items[i].id == _id){
           items[i].select();
           items[i].touchEnabled = false;
           items[i].bubbleParent = false;
        }else{
           items[i].deselect();
           items[i].touchEnabled = true;
           items[i].bubbleParent = true;
        }
    }
}

function deselectAll(){
    var items = $.items.getChildren();
    for(i in items){
        items[i].deselect();
        items[i].touchEnabled = true;
        items[i].bubbleParent = true;
    }
}

function imageSelectedMethod(_image,_method){
    switch(_method){
        default:
        case "opacity":
            var imageSelectedParam = {
                src: _image,
                opacitySelected: 1,
                opacityDeselected: 0.5,
            };
        break;
        
        case "file":
            var icn = _image.split(".");
            var imageSelectedParam = {
                src: icn[0]+"_selected."+icn[1],
                opacitySelected: 1,
                opacityDeselected: 1,
            };
        break;
    }
    return imageSelectedParam;
}
