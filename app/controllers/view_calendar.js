// LOAD LIBRARIES

var CALENDAR = require("orange_plugins/basic/calendar");

// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "Calendar"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function(){
    CALENDAR.init();
};

$.vwCalendar.reset = function(){
    
};

$.vwCalendar.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};

function calendar_getAll(){
    var cal_list = CALENDAR.getAllCalendar();
    for (var i = 0; i < cal_list.length; i++) {
        $.debug("#calendar_getAll > "+"cid:"+cal_list[i].id+" name:"+cal_list[i].name);
    }
}

function calendar_addEvent(){    
  
    var evt = {
        title: "Innovation",
        location:"Think Do",
        //allDay: false,
        begin: CALENDAR.moment().add('h',1).toDate(),
        end: CALENDAR.moment().add('h',3).toDate(),
        //notes:"Session 44",   
    };
    var event_id = CALENDAR.addEvent(evt);
    
    
    if(OS_IOS){
        var alt = [
            { 
                absoluteDate: CALENDAR.moment().add('m',15).toDate() 
            },
            { 
                absoluteDate: CALENDAR.moment().add('m',25).toDate() 
            }
        ];
        CALENDAR.setAlert(event_id,alt);
        
        var rec = [{
            frequency: CALENDAR.api.RECURRENCEFREQUENCY_WEEKLY,
            interval: 1,
            end: {occurrenceCount:5}
        }];
        CALENDAR.setRecurrent(event_id,rec);   
    }



    if(OS_ANDROID){
        var rmd = [
            {
                method: CALENDAR.api.METHOD_ALERT, 
                minutes: 30
            },
            {
                method: CALENDAR.api.METHOD_ALERT, 
                minutes: 5
            }
        ];
        CALENDAR.setReminder(event_id,rmd);       
    }



    $.debug("#calendar_addEvent > "+"event_id:"+event_id);
}

// RUNTIME
$.init();