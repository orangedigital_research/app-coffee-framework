// LOAD LIBRARIES
var FACEBOOK = require("orange_plugins/social/fb");

// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "Facebook"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);
$.NavigationTopBar.setRight({
    visible:true,
    image:null,
    text:"Share",
    callback:facebook_share
});

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function(){  
      
    var fb_access = FACEBOOK.init(APP.FB_APP_ID);
    if(fb_access == 0 || fb_access == null){
        FACEBOOK.grantAccess();
    }
    
    if(OS_ANDROID && FACEBOOK.api.loggedIn){ 
        $.debug("eventViewFacebookLogin (init)");
        FACEBOOK.getFriends(facebook_friendlist);
    }
        
    
};

$.vwFacebook.reset = function(){
    
};

$.vwFacebook.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};

function facebook_friendlist(friendlist){
    var friendset = Array();
    $.debug("facebook_friendlist > "+friendlist.length);
    for(f=0; f<friendlist.length; f++){
        
        if(OS_IOS){
            var friendRow = Ti.UI.createTableViewRow({           
                title: friendlist[f].first_name+" "+friendlist[f].last_name,
                rightImage: "http://graph.facebook.com/"+friendlist[f].id+"/picture/",
                fbid: friendlist[f].id
            });            
        }

        if(OS_ANDROID){
            var friendRow = Ti.UI.createTableViewRow({           
                title: friendlist[f].first_name+" "+friendlist[f].last_name,
                fbid: friendlist[f].id
            });            
        }
        
        friendRow.addEventListener("click",function(e){
            var fb_friend_id = e.row.fbid;
            facebook_invite(fb_friend_id);
        });
        
        friendset.push(friendRow);
    }
    $.tblFriends.setData(friendset);
}

function facebook_invite(fbid){
    $.debug("facebook_invite > "+fbid);
    var param = {
        fbid: fbid,
        message: "Facebook Invite Dialog - Coffee",
        success: function(){
            $.debug("facebook_invite > success");
        },
        error: function(){
            $.debug("facebook_invite > failed");
        }
    };          
    FACEBOOK.invite(param);
}

function facebook_share(){
    var param = {
        graph_url: "feed",
        success:function(){
            $.debug("facebook_share > success");
            $.NavigationTopBar.showRight();            
        },
        error:function(){
            $.debug("facebook_share > error");
            $.NavigationTopBar.showRight();            
        }
    }; 
     
    var data = {
        name: "Coffee Framework", 
        link: "http://coffee.com.au", 
        caption: "FB Caption",
        description: "FB Description",
        picture: "http://www.communitymatters.org/sites/default/files/sandbox_credit-2greenthumbsup.com_.jpg",
        message: "FB message",
    }; 
    
            
    FACEBOOK.share(param,data);
    $.NavigationTopBar.hideRight();
}

// LISTENERS
function eventViewFacebookLogin(e){
    FACEBOOK.getFriends(facebook_friendlist);
    $.debug("eventViewFacebookLogin (event)");
}
FACEBOOK.api.removeEventListener('login', eventViewFacebookLogin);
FACEBOOK.api.addEventListener('login', eventViewFacebookLogin);

// RUNTIME
$.init();