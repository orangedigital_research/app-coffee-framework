// LOAD LIBRARIES
var MAP = require("orange_plugins/location/map");
var GPS = require("orange_plugins/location/gps");


// CONFIF HEADER
var _param = APP.default_navigationTopBar;
_param.title = "Map & GPS";
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);
$.NavigationTopBar.setSettings(map_showActions);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.modMap.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function() {
    MAP.init($.modMap);
    map_mylocation();
};

$.vwMap.reset = function() {

};

$.vwMap.restart = function() {

};

$.debug = function(string) {
    $.debugOutput.value = $.debugOutput.value + "\n" + string;
};

function map_showActions(){
    var optionSettingsChoice = {
        options: ['Map Mode', 'Add Pin', 'Locate me', 'Locate my address','Geodecoder','Cancel'],
        cancel: 5,
    };
    
    var optionSettings = Titanium.UI.createOptionDialog(optionSettingsChoice);
    optionSettings.addEventListener('click', function(e) {
        switch (e.index) {
            case 0:
                map_switchmode();
                break;

            case 1:
                map_addPin();
                break;

            case 2:
                map_mylocation();
                break;

            case 3:
                map_myaddress();
                break;

            case 4:
                map_geodecoder();
                break;                
                
            case 5: //Cancel
                break;
        }
    });

    optionSettings.show();
}

function map_longClick(e) {

    if(OS_IOS){
       var coordinate =  MAP.convertPixeltoLatLng(e.x, e.y);
    }
    
    if(OS_ANDROID){
       var coordinate =  {latitude: e.latitude, longitude: e.longitude};
    }
    
    var a = MAP.api.createAnnotation({
        latitude: coordinate.latitude,
        longitude: coordinate.longitude,
        title: "New Place",
        pincolor: MAP.api.ANNOTATION_GREEN,
        animate: true,
        draggable: true
    });

    MAP.mapView.addAnnotation(a);
}

function map_dragPin(e) {
    if (e.oldState == MAP.api.ANNOTATION_DRAG_STATE_END) {
        $.debug("#map_dragPin > lat: " + e.annotation.latitude + " - lng: " + e.annotation.longitude);
    }
}

function map_addPin() {
    var a = MAP.api.createAnnotation({
        latitude: MAP.mapView.region.latitude,
        longitude: MAP.mapView.region.longitude,
        title: "New Annotation",
        pincolor: MAP.api.ANNOTATION_RED,
        animate: true,
        draggable: true
    });

    MAP.mapView.addAnnotation(a);
    $.debug("#map_addPin > lat: " + MAP.mapView.region.latitude + " - lng: " + MAP.mapView.region.longitude);
}

function map_mylocation() {
    GPS.getPosition(function(user_coordinate) {
        $.debug("#map_mylocation > " + JSON.stringify(user_coordinate));
        MAP.setRegion(user_coordinate.latitude, user_coordinate.longitude, 0.01);
    });
}

function map_myaddress() {
    GPS.getReverseGeocoder(function(places) {
        $.debug("#map_myaddress > " + JSON.stringify(places));
    });
}

function map_geodecoder() {
    var address = "4 Petrie Terrace, Brisbane, Australia";
    GPS.getGeocoder(address, function(geocoder) {
        $.debug("#map_geodecoder >  4 Petrie Terrace, Brisbane, Australia :: " + JSON.stringify(geocoder));
        MAP.setRegion(geocoder.latitude, geocoder.longitude, 0.01);
    });
}

function map_switchmode() {
    MAP.pickType();
}

// LISTENERS


// RUNTIME
$.init();