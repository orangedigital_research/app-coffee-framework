// LOAD LIBRARIES
var FILESYSTEM = require("orange_plugins/system/filesystem");


// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "File System"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};
var sandbox = {
    pid: null,
    gid: null
};

// FUNCTIONS
$.init = function(){

};

$.vwFilesystem.reset = function(){
    
};

$.vwFilesystem.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};

function filesystem_filelist(){
    var list = FILESYSTEM.getDirectoryListing("");
    for(i=0; i<list.length; i++){
        $.debug("#filesystem_filelist > "+list[i]);
    }
}

function filesystem_saveFile(){
    fn = $.txtName.value;
    fc = $.txtContent.value;
    
    FILESYSTEM.saveFile({
       path:  fn,
       content: fc,
       append: true,
       success: function(){
           $.debug("#filesystem_saveFile > "+fn);
           
           $.txtName.value = "";
           $.txtContent.value = "";
           
           $.txtName.blur();
           $.txtContent.blur();
       }
    });
}

function filesystem_move(){
    fn = $.txtName.value;
    fc = $.txtContent.value;
    
    FILESYSTEM.move({
       path:  fn,
       new_path: fc,
       success: function(){
           $.debug("#filesystem_move > "+fn+" >> "+fc);
           
           $.txtName.value = "";
           $.txtContent.value = "";
           
           $.txtName.blur();
           $.txtContent.blur();
       },
       error: function(){
           $.debug("#filesystem_move!");
       }
    });   
}

function filesystem_rename(){
    fn = $.txtName.value;
    fc = $.txtContent.value;
    
    FILESYSTEM.rename({
       path:  fn,
       new_name: fc,
       success: function(){
           $.debug("#filesystem_rename > "+fn+" >> "+fc);
           
           $.txtName.value = "";
           $.txtContent.value = "";
           
           $.txtName.blur();
           $.txtContent.blur();
       },
       error: function(){
           $.debug("#filesystem_rename!");
       }
    });   
}

function filesystem_removeFile(){
    fn = $.txtName.value;
    FILESYSTEM.removeFile({
       path:  fn,
       success: function(){
           $.debug("#filesystem_removeFile > "+fn);
           
           $.txtName.value = "";
           
           $.txtName.blur();
       },
       error: function(){
           $.debug("#filesystem_removeFile!");
       }
    });
}

function filesystem_createDirectory(){
    dn = $.txtName.value;
    
    FILESYSTEM.createDirectory({
       path:  dn,
       success: function(){
           $.debug("#filesystem_createDirectory > "+dn);
           
           $.txtName.value = "";
           
           $.txtName.blur();
       }
    });  
}

function filesystem_removeDirectory(){
    dn = $.txtName.value;
    FILESYSTEM.removeDirectory({
       path:  dn,
       success: function(){
           $.debug("#filesystem_removeDirectory > "+dn);
           
           $.txtName.value = "";
           
           $.txtName.blur();
       },
       error: function(){
           $.debug("#filesystem_removeDirectory!");
       }
    });    
}

// LISTENERS


// RUNTIME
$.init();