// LOAD LIBRARIES
var GESTURE = require("orange_plugins/sensors/gesture");
var ACCELEROMETER = require("orange_plugins/sensors/accelerometer");


// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "Movement"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function(){ 
  
    GESTURE.init(gesture_callback);
    
    ACCELEROMETER.init(accelerometer_callback);

};

$.vwMovement.reset = function(){
    
};

$.vwMovement.restart = function(){
    
};

$.debug_gt = function(string){
    $.debugOutput_gt.value = string;
};

$.debug_acl = function(string){
    $.debugOutput_acl.value = string;
};

function gesture_callback(e){
    $.debug_gt("GESTURE > "+JSON.stringify(e));    
}

function accelerometer_callback(e){
    $.debug_acl("ACCELEROMETER > "+JSON.stringify(e));    
}

// RUNTIME
$.init();