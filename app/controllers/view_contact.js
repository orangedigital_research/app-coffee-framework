// LOAD LIBRARIES
var CONTACT             = require("orange_plugins/basic/contact");
var PHONE               = require("orange_plugins/network/phone");
var EMAIL               = require("orange_plugins/network/email");
var SMS                 = require("orange_plugins/network/sms");
var DATABASE            = require("orange_plugins/system/database");

// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "Contact"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}


// PASSED PARAMETERS
var args = arguments[0] || {};
var sandbox = {
    pid: null,
    gid: null
};

var vcard = {
  firstName: 'Paul',
  lastName: 'Dowsett',
  address:{
    work:[
      {
        CountryCode: 'gb', // determines how the address is displayed
        Street: '200 Brook Drive\nGreen Park',
        City: 'Reading',
        County: 'Berkshire',
        Country: 'England',
        ZIP: 'RG2 6UB'
      },
      {
        CountryCode: 'gb', // determines how the address is displayed
        Street: '1 St Pauls Road\nClerkenwell',
        City: 'City of London',
        State: 'London',
        Country: 'England',
        ZIP: 'EC1 1AA'
      }
    ],
    home:[
      {
        CountryCode: 'gb', // determines how the address is displayed
        Street: '2 Boleyn Court',
        City: 'London',
        State: 'Greenwich',
        Country: 'England',
        ZIP: 'SE10'
      }
    ]
  },
  birthday: '2012-01-01T12:00:00.000+0000',
  instantMessage:{
    home:[
      {
        service: 'AIM',
        username: 'leisureAIM'
      },
      {
        service: 'MSN',
        username: 'no_paul_here@msn.com'
      }
    ],
    work:[
      {
        service: 'AIM',
        username: 'seriousAIM'
      }
    ]
  },
  organization: 'Appcelerator',
  phone:{
    mobile: ['07900 000001', '07900 000002'],
    work: ['+44 (0)118 925 6128', '+44 (0)118 000 0000']
  },
  url:{
    homepage: ['www.google.com'],
    work: ['www.appcelerator.com', 'www.example.com']
  }
};

// FUNCTIONS
$.init = function(){
    CONTACT.init();
    
    //database
    DATABASE.reinstall("/database/sandbox.sqlite", "sandbox_db");
    DATABASE.close();
};

$.vwContact.reset = function(){
    
};

$.vwContact.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};

function contact_saveDatabase(person){
    DATABASE.open("sandbox_db");
    DATABASE.query({sql:"BEGIN"});
    
    if(OS_IOS){
        var param = {
            sql: "INSERT INTO contacts (cid, fullname) VALUES (?, ?)",
            values: [person.recordId, person.fullName]
        };         
    }else{
        var param = {
            sql: "INSERT INTO contacts (cid, fullname) VALUES (?, ?)",
            values: [person.id, person.fullName]
        };         
    }

    DATABASE.query({sql:"COMMIT"});
    DATABASE.query(param);
    DATABASE.close();
}

function contact_picker(){
    CONTACT.pick({
        cancel:function(e){
            
        },
        selectedPerson:function(e){
            var person = e.person;
            if(OS_IOS){
                $.lblContact.text = person.fullName+" (cid:"+person.recordId+")";
                $.debug("#contact_picker > cid:"+person.recordId);
                sandbox.pid = person.recordId;                
            }else{
                $.lblContact.text = person.fullName+" (cid:"+person.id+")";
                $.debug("#contact_picker > cid:"+person.id);
                sandbox.pid = person.id;                
            }
        }
    });
}

function contact_personlist(){
    var list = CONTACT.getAllPerson();
    for(i=0; i<list.length; i++){
        $.debug("#contact_personlist > "+JSON.stringify(list[i].fullName));
        contact_saveDatabase(list[i]);
    }  
}

function contact_grouplist(){
    var list = CONTACT.getAllGroup();
    for(i=0; i<list.length; i++){
        $.debug("#contact_grouplist > "+JSON.stringify(list[i].name));
    }   
}

function contact_addperson(){
    CONTACT.addPerson({
        person: vcard,
        callback: function(person){
            if(OS_IOS){
                $.debug("#contact_addperson > "+person.fullName+" (cid:"+person.recordId+")");
                $.lblContact.text = person.fullName+" (cid:"+person.recordId+")";
                sandbox.pid = person.recordId;                
            }else{
                $.debug("#contact_addperson > "+person.fullName+" (cid:"+person.id+")");
                $.lblContact.text = person.fullName+" (cid:"+person.id+")";
                sandbox.pid = person.id;                                
            }           
        }
    });
}

function contact_removeperson(){
    CONTACT.removePerson({
        person: CONTACT.getPerson(sandbox.pid),
        callback: function(){
            $.debug("#contact_removeperson > Contact cid:"+sandbox.pid+" deleted");
            $.lblContact.text = "";
            sandbox.pid = null;
        }
    });
}

function contact_addgroup(){
    var today = new Date();
    var group = {
        name: 'New Group '+today.getTime()
    }; 
    
    CONTACT.addGroup({
        group: group,
        callback: function(group){
            $.debug("#contact_addgroup > "+group.name+" (gid:"+group.recordId+")");
            $.lblContact.text = group.name+" (gid:"+group.recordId+")";
            sandbox.gid = group.recordId;
        }
    });
}


function contact_removegroup(){
    CONTACT.removeGroup({
        group: CONTACT.getGroup(sandbox.gid),
        callback: function(){
            $.debug("#contact_removegroup > Group gid:"+sandbox.gid+" deleted");
            $.lblContact.text = "";
            sandbox.gid = null;
        }
    });
}

function contact_email(){
    var person = CONTACT.getPerson(sandbox.pid);
    var person_emails = CONTACT.getAllEmails(person.email);
    
    EMAIL.pick({
        emails:person_emails,
        subject: "Invite Email",
        body: "Hello World",
        cancel:function(){}
    });
    
}

function contact_sms(){
    var person = CONTACT.getPerson(sandbox.pid);
    var person_phones = CONTACT.getAllPhones(person.phone);
    
    SMS.pick({
        phones:person_phones,
        message: "Hello world",
        cancel:function(){}
    }); 
}

function contact_call(){
    var person = CONTACT.getPerson(sandbox.pid);
    var person_phones = CONTACT.getAllPhones(person.phone);
    
    PHONE.pick({
        phones:person_phones,
        cancel:function(){}
    });     
}

// LISTENERS


// RUNTIME
$.init();
