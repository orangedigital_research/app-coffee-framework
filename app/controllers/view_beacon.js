// LOAD LIBRARIES
if (OS_IOS){
    var BEACON      = require("orange_plugins/location/beacon");
    var BGSERVICE   = require("orange_plugins/system/bgservice");    
}



// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "Beacon"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function(){
    if(OS_IOS){
        var param = {
            autoRanging:true,       
            advertise: function(e){
                beacon_advertise(e);
            },          
            enteredRegion: function(e){
                beacon_enterRegion(e);
            },
            exitedRegion: function(e){
                beacon_exitRegion(e);
            },
            determinedRegionState: function(e){
                beacon_stateRegion(e);
            },
            ranges: function(e){
                beacon_range(e);
            },               
            proximity: function(e){
                beacon_proximity(e);
            } 
            
        };
        
        BEACON.init(param); 
    }else{
        $.debug("init > This device do not support beacon");
    }    
};

$.vwBeacon.reset = function(){
    
};

$.vwBeacon.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};

function beacon_startEmiter(){
    if(OS_IOS){
        if($.swtEmiter.value){
            var emiter_param = {
               uuid : $.txtUUID.value,
               identifier : $.txtIdentifier.value,
               major: Number($.txtMajor.value),
               minor: Number($.txtMinor.value)
            };
            BEACON.startEmiter(emiter_param);
            $.debug("beacon_startEmiter > start");  
        }else{
            BEACON.stopEmiter();
            $.debug("beacon_startEmiter > off");
        }
    }else{
        $.debug("beacon_startEmiter > This device do not support beacon");
    }    
}

function beacon_startScan(){
    if(OS_IOS){
        if($.swtScan.value){
            
            
            var scan_estimote_purple = {
               uuid : "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
               identifier : "purple",
               major: 55643,
               minor: 5863
            };
            BEACON.setBeacon(scan_estimote_purple);
            
    
            var scan_estimote_green = {
               uuid : "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
               identifier : "green",
               major: 26910,
               minor: 47392
            };
            
    
            var scan_estimote_blue = {
               uuid : "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
               identifier : "blue",
               major: 60633,
               minor: 35950
            };
            
            BEACON.startScanner(scan_estimote_purple);
            BEACON.startScanner(scan_estimote_green);
            BEACON.startScanner(scan_estimote_blue);
        
        }else{
            BEACON.stopScanner();
        }
    }else{
        $.debug("This device do not support beacon");
    }
}

function beacon_advertise(data){
    $.debug("beacon_adverstise > "+ JSON.stringify(data.status));
}

function beacon_enterRegion(data){
    $.debug("beacon_enterRegion > "+ JSON.stringify(data));
}

function beacon_exitRegion(data){
    $.debug("beacon_exitRegion > "+ JSON.stringify(data));
}

function beacon_stateRegion(data){
    $.debug("beacon_stateRegion > "+ JSON.stringify(data));
}

function beacon_range(data){
    $.debug("beacon_range > "+ JSON.stringify(data.beacons));
    for(b=0; b<data.beacons.length;  b++){
        var identifier = data.beacons[b].uuid+"_"+data.beacons[b].major+"_"+data.beacons[b].minor;        
        beacon_color(data.beacons[b].proximity, identifier, Number(data.beacons[b].accuracy) );
    }    
}

function beacon_proximity(data){
    $.debug("beacon_proximity > "+ JSON.stringify(data));
    //beacon_color(data.proximity);
}

function beacon_color(proximity, identifier, accuracy){
    $.debug("beacon_color > identifier > "+ identifier);
    $.debug("beacon_color > proximity > "+ proximity);
    if(accuracy < 1){
        switch(proximity){
            case "far":
                $.vwBeacon.backgroundColor = "#fff";
            break;
            
            case "near":
                switch(identifier){
                     case "B9407F30-F5F8-466E-AFF9-25556B57FE6D_55643_5863": // purple
                        $.vwBeacon.backgroundColor = "#dea9ff";
                     break;                 
                     case "B9407F30-F5F8-466E-AFF9-25556B57FE6D_26910_47392": // green
                        $.vwBeacon.backgroundColor = "#dcffb2";
                     break;                 
                     case "B9407F30-F5F8-466E-AFF9-25556B57FE6D_60633_35950": // blue
                        $.vwBeacon.backgroundColor = "#b2e7ff";
                     break;
                }
            break;
            
            case "immediate":
                switch(identifier){
                     case "B9407F30-F5F8-466E-AFF9-25556B57FE6D_55643_5863": // purple
                        $.vwBeacon.backgroundColor = "#dea9ff";
                     break;                 
                     case "B9407F30-F5F8-466E-AFF9-25556B57FE6D_26910_47392": // green
                        $.vwBeacon.backgroundColor = "#dcffb2";
                     break;                 
                     case "B9407F30-F5F8-466E-AFF9-25556B57FE6D_60633_35950": // blue
                        $.vwBeacon.backgroundColor = "#b2e7ff";
                     break;
                }
            break;
            
            default:
            case "unknown":
            break;       
            
    }
    }
}


if(OS_IOS){
    // LISTENERS
    BGSERVICE.register("bgservice_beacon.js");
    
    // RUNTIME
    $.init();
}else{
    $.debug("This device do not support beacon");
}
