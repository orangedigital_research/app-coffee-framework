// LOAD LIBRARIES
var BONJOUR = require("orange_plugins/network/bonjour");

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function(){
    BONJOUR.init('hello',function(e){
        BONJOUR.Log('debug','callback > '+JSON.stringify(e));
    });
    BONJOUR.start();
};

$.vwBonjour.reset = function(){
    
};

$.vwBonjour.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = string+"\n"+$.debugOutput.value;
};

function bonjour_sendData(){
    BONJOUR.send('hello world');
}

// LISTENERS


// RUNTIME
$.init();