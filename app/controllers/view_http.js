// LOAD LIBRARIES
var HTTP = require("orange_plugins/network/http");


// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "HTTP Request"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function(){
    param = {   
        url: "http://www.langports.com/?lp_api=getAll&sid=0", 
        format:"json",
        success: function(data, url){
            request_success(data);
        },
        failure: function(){
            request_error();
        }
    };   
    
    HTTP.request(param);
};

$.vwHttp.reset = function(){
    
};

$.vwHttp.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = string;
};

function request_success(data){
    $.debug("HTTP > "+JSON.stringify(data));
}

function request_error(){
    $.debug("HTTP > error");
}


// LISTENERS


// RUNTIME
$.init();