// LOAD LIBRARIES
var SOCKET = require("orange_plugins/network/socket");

// PASSED PARAMETERS
var args = arguments[0] || {};

var socket_server = null;
var socket_client = null;

// FUNCTIONS
$.init = function(){

};

$.vwSocket.reset = function(){
    
};

$.vwSocket.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};

function socket_createServer(){
   var paramSocketServer = {
        host: SOCKET.host,
        port: 88888,
        timeout:10000,
        accepted: function(e){
            socket_accepted(e);
        },
        error:function(e){
            socket_error(e);
        }
    };    
    socket_server = SOCKET.openTCP(paramSocketServer);
    socket_server.listen();
    socket_server.accept({
        timeout : 60000
    });
    SOCKET.log("info","Socket > socket_server > Listening...");
}

function socket_createClient(){
   var paramSocketClient = {
        host: SOCKET.host,
        port: 88888,
        timeout:10000,
        connected:function(e){
            socket_connected(e);
        },
        error:function(e){
            socket_error(e);
        }
    };    
    socket_client = SOCKET.openTCP(paramSocketClient);
    socket_client.connect();
    SOCKET.log("info","Socket > socket_client > Connecting...");    
}


function request_success(data){
    $.txtResult.value = JSON.stringify(data);
}

function socket_accepted(e){
    SOCKET.log("info","Listening socket <" + e.socket + "> accepted incoming connection <" + e.inbound + ">");
}

function socket_connected(e){
    SOCKET.log("info","Socket Connected");
    Ti.Stream.pump(e.socket, socket_pump, 1024, true);
    e.socket.write(Ti.createBuffer({
        value : 'A message from a connecting socket.'
    }));
}

function socket_error(e){
    SOCKET.log("error","Socket Error > "+e.errorCode+" - "+e.error);
}

function socket_pump(e) {
    if (e.bytesProcessed < 0) {
        SOCKET.log("info","Closing client socket.");
        //clientSocket.close();
        return;
    }
    try {
        if(e.buffer) {
            var received = e.buffer.toString();
            SOCKET.log("info","'Received: ' "+ received);
        } else {
            SOCKET.log("error","Error: read callback called with no buffer!");
        }
    } catch (ex) {
        Ti.API.error(ex);
    }
}

// LISTENERS


// RUNTIME
$.init();