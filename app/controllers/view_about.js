// LOAD LIBRARIES

// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "About"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

// FUNCTIONS
$.init = function(){     
    $.debug("Version: "+APP.CORE.app.version);
    $.debug("Screen: "+APP.CORE.width+"x"+APP.CORE.height+" ("+APP.CORE.density+"dpi@"+APP.CORE.densityFactor+")");
};

$.vwAbout.reset = function(){
    
};

$.vwAbout.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};


// LISTENERS


// RUNTIME
$.init();