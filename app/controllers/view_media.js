// LOAD LIBRARIES
var FILESYSTEM  = require("orange_plugins/system/filesystem");
var PICTURE     = require("orange_plugins/media/picture");
var CAMERA      = require("orange_plugins/media/camera");


$.imgMedia.width = APP.CORE.width;
$.videoPlayer.width = APP.CORE.width;

// CONFIF HEADER
var _param      = APP.default_navigationTopBar;
_param.title    = "Camera & Picture"; 
$.NavigationTopBar.init(_param);
$.NavigationTopBar.setMenu(APP.toggleMenu);

if (OS_IOS && APP.CORE.device.os.major >= 7) {
    $.vwContent.top = 70;
}

// PASSED PARAMETERS
var args = arguments[0] || {};

// FUNCTIONS
$.init = function(){

};

$.vwMedia.reset = function(){
    
};

$.vwMedia.restart = function(){
    
};

$.debug = function(string){
    $.debugOutput.value = $.debugOutput.value+"\n"+string;
};

function media_image_picker(){
    var param = {
        allowEditing: true,
        popoverView:null,
        mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
        success: function(media){
            $.imgMedia.image = media;
            $.debug("media_image_picker");
        },
        cancel: function(){
            $.debug("media_image_picker > Cancel");
        },
        error: function(){
            $.debug("media_image_picker > No access to Media");
        }       
    };
    PICTURE.pick(param);
}

function media_image_screenshot(){
    PICTURE.screenshot($.vwMedia);
    $.debug("media_image_screenshot");
}

function media_camera_takePicture(){
    var param = {
        showControls: true,
        allowEditing: true,
        saveToPhotoGallery:true,
        mediaTypes: [Ti.Media.MEDIA_TYPE_VIDEO, Ti.Media.MEDIA_TYPE_PHOTO],
        videoQuality: Titanium.Media.QUALITY_HIGH,
        videoMaximumDuration: 15000,
        success: function(event_media){
            
            switch(event_media.mediaType){
                case Ti.Media.MEDIA_TYPE_PHOTO:                
                    if(!FILESYSTEM.checkExists("picture.jpg")){

                       FILESYSTEM.saveFile({
                            path: "picture.jpg",
                            content: event_media.media,
                            append:false,
                            success:function(){
                                                                
                                var mediaFile = FILESYSTEM.getFile("picture.jpg");
                                $.videoPlayer.opacity = 0;
                                $.imgMedia.opacity = 1;
                                $.imgMedia.image = null;
                                $.imgMedia.image = mediaFile;
                                                                
                            }
                        });
                        
                    }else{
 
                         FILESYSTEM.removeFile({
                           path: "picture.jpg",
                           success:function(){
                               
                               FILESYSTEM.saveFile({
                                    path: "picture.jpg",
                                    content: event_media.media,
                                    append:false,
                                    success:function(){
                                                                        
                                        var mediaFile = FILESYSTEM.getFile("picture.jpg");
                                        $.videoPlayer.opacity = 0;
                                        $.imgMedia.opacity = 1;
                                        $.imgMedia.image = null;
                                        $.imgMedia.image = mediaFile;
                                    }
                                });
                               
                               
                           },
                           error: function(){
                               
                           }
                        });
 
                        
                    }
                    
                    $.debug("media_camera_takePicture > picture");                        
                break;
                
                case Ti.Media.MEDIA_TYPE_VIDEO:
                
                    if(!FILESYSTEM.checkExists("video.mov")){

                       FILESYSTEM.saveFile({
                            path: "video.mov",
                            content: event_media.media,
                            append:false,
                            success:function(){
                                                                
                                var mediaFile = FILESYSTEM.getFile("video.mov");
                                $.imgMedia.opacity  = 0;
                                $.videoPlayer.opacity = 1;
                                $.videoPlayer.media = null;
                                $.videoPlayer.media = mediaFile;
                                $.videoPlayer.play();
                                
                                mediaFile = null;
                            }
                        });
                        
                    }else{
 
                         FILESYSTEM.removeFile({
                           path: "video.mov",
                           success:function(){
                               
                               FILESYSTEM.saveFile({
                                    path: "video.mov",
                                    content: event_media.media,
                                    append:false,
                                    success:function(){
                                                                        
                                        var mediaFile = FILESYSTEM.getFile("video.mov");
                                        $.imgMedia.opacity = 0;
                                        $.videoPlayer.opacity = 1;
                                        $.videoPlayer.media = null;
                                        $.videoPlayer.media = mediaFile;
                                        $.videoPlayer.play();
                                        
                                        mediaFile = null;
                                    }
                                });
                               
                               
                           },
                           error: function(){
                               
                           }
                        });
 
                        
                    }
                    


                    $.debug("media_camera_takePicture > video");  
                
                break;
                
                default:
                    $.debug("media_camera_takePicture > Wrong Media Type");
                break;
            }
                 
            event_media = null;     
        },
        cancel: function(){
            $.debug("media_camera_takePicture > cancel");  
        },
        error: function(){
            $.debug("media_camera_takePicture > No access to Camera");  
        }       
    };
    CAMERA.show(param);
}

// LISTENERS


// RUNTIME
$.init();