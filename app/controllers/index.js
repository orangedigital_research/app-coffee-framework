$.init = function(){
    APP.CORE.init();
    APP.STACK.init( $.vwMainstage );
    APP.STACK.loadView('view_about', {transition: false});

    $.vwMainstage.width     = APP.STACK.width;
    $.vwMainstage.height    = APP.STACK.height; 
  
    APP.param_navigationSideBar.mainstage   = $.vwMainstage;
    APP.param_navigationSideBar.callback    = index_sidemenu_callback;    
    $.NavigationSideBar.init(APP.param_navigationSideBar);   
  
    $.index.open();
};

APP.toggleMenu = function(){
    $.NavigationSideBar.toggle();
};
    
function index_sidemenu_callback(e){
    APP.STACK.loadView(e.view, {transition: false, cache: true, restart:true});
    $.NavigationSideBar.toggle();
}

function index_tabmenu_callback(e){
    APP.STACK.loadView(e.view, {transition: false, cache: true, restart:true});
}

function eventIndexOrientation(e){
    $.vwMainstage.width     = APP.STACK.width;
    $.vwMainstage.height    = APP.STACK.height; 
}
Titanium.Gesture.removeEventListener('orientationchange', eventIndexOrientation);
Titanium.Gesture.addEventListener('orientationchange', eventIndexOrientation);

function eventPushNotificationRegistered(e){
    APP.PUSHNOTIFICATION.subscribe('coffee');
}
Titanium.App.addEventListener("pushnotification:registered",eventPushNotificationRegistered);

function eventPushNotificationReceived(e){
    var push_message =  e.notification.alert;  
    
    var push_alt = Ti.UI.createAlertDialog({
        message: push_message,
        ok: 'OK',
        title: 'Push Notification'
    }).show();
    push_alt = null;  
}
Titanium.App.addEventListener("pushnotification:received",eventPushNotificationReceived);


if(OS_ANDROID){   
         
    var observerAndroidBack = function(){
        APP.actionAndroidBack();
    };
    $.index.addEventListener('androidback', observerAndroidBack);
    
}

$.init();