SANDBOX = {
    
};

APP = {
    CORE: require("orange_plugins/basic/core"),
    STACK: require("orange_plugins/basic/stack"),
    PUSHNOTIFICATION: require("orange_plugins/network/pushnotification"),
    
    FB_APP_ID: 543763029065267,
   
    actionAndroidBack: function(){},
    
    default_navigationTopBar:{
        title:"",
        backgroundColor:"#ff3600",
        backgroundGradient:{
            type: 'linear',
            startPoint: { x: '0%', y: '0%' },
            endPoint: { x: '0%', y: '100%' },
            colors: [ { color: '#ff3600', offset: 0.0}, { color: '#ff3600', offset: 1.0 } ],
        },
        fontColor:"#ffffff",
        fontFamily: "HelveticaNeue-Regular",
        fontSize: 20,
        buttonTint:"#ffffff",       
    },

    param_navigationSideBar:{
        items:[
            {
                id:"tab1",
                title:"Contact",
                view:"view_contact",
                icon:"/images/icon_contact.png",
                selected:false
            },        
            {
                id:"tab2",
                title:"Calendar",
                view:"view_calendar",
                icon:"/images/icon_calendar.png",
                selected:false
            },
            {
                id:"tab3",
                title:"Map & GPS",
                view:"view_map",
                icon:"/images/icon_map.png",
                selected:false
            },
            {
                id:"tab4",
                title:"Camera & Picture",
                view:"view_media",
                icon:"/images/icon_media.png",
                selected:false
            },
            {
                id:"tab5",
                title:"File System",
                view:"view_filesystem",
                icon:"/images/icon_filesystem.png",
                selected:false
            },
            {
                id:"tab6",
                title:"HTTP Request",
                view:"view_http",
                icon:"/images/icon_http.png",
                selected:false
            },    
            {
                id:"tab7",
                title:"Facebook",
                view:"view_facebook",
                icon:"/images/icon_facebook.png",
                selected:false
            },              
            {
                id:"tab8",
                title:"Movement",
                view:"view_movement",
                icon:"/images/icon_movement.png",
                selected:false
            },                   
            {
                id:"tab9",
                title:"Beacon",
                view:"view_beacon",
                icon:"/images/icon_beacon.png",
                selected:false
            }, 
        ],
        defaultParam:{
            left:0,
            right:0,
            backgroundColor:"#333333",
            backgroundColorSelected:"#222222",
            seperatorColor: "#4d4d4d",
            fontSize:14,
            fontFamily:"HelveticaNeue-Light",
            fontColor:"#999999",
            fontColorSelected:"#ffffff",
            iconSelectedMethod:"opacity"
        },
        width: 270,
        height:250,
        position:"left",
        zindex:"under",
        sfx:null,
        backgroundColor:"#333333",      
        callback:null
    },

    
    param_navigationTabBar:{
        items:[
            {
                id:"tab1",
                title:"Contact",
                view:"view_contact",
                icon:"/images/icon_tab1.png",
                selected:true
            },        
            {
                id:"tab2",
                title:"Calendar",
                view:"view_calendar",
                icon:"/images/icon_tab1.png",
                selected:false
            },
            {
                id:"tab3",
                title:"Map",
                view:"view_map",
                icon:"/images/icon_tab1.png",
                selected:false
            },
            {
                id:"tab4",
                title:"Facebook",
                view:"view_facebook",
                icon:"/images/icon_tab1.png",
                selected:false
            },
        ],
        defaultParam:{
            backgroundColorSelected:"#000000",
            fontFamily:"HelveticaNeue-Light",
            fontColor:"#efefef",
            fontColorSelected:"#ffffff",
            iconSelectedMethod:"opacity"           
        },
        backgroundColor:"#333333",
        callback:null
    },
    
    
    param_navigationPages:{
        items:[
            {
                id:"step1",
                title:"Step 1",
                selected:false
            },
            {
                id:"step2",
                title:"Step 2",
                selected:true
            },
            {
                id:"step3",
                title:"Step 3",
                selected:false
            },
            {
                id:"step4",
                title:"Step 4",
                selected:false
            },
        ],
        defaultParam:{
            clickable:true,
            primaryColor:"#666",
            bulletColor:"#ffffff",
            bulletColorSelected:"#ffcc00",
            bulletSize:16,
            bulletRadius:8,
            bulletBorder:1,
            labelPosition:"bottom",
            fontFamily:"HelveticaNeue-Light",
            fontColor:"#444",        
        },
        padding:10,
        extend:true,     
        callback:null
    },
};

Alloy.Globals.MAP = require('ti.map');
APP.PUSHNOTIFICATION.register();
