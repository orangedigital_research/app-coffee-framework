// NETWORK
// --------------------
// Use this plugin in your app:
//
// `var HTTP = require('/plugin/network/http');`
//
// `HTTP.request(param);`
//
// Use CFBundleURLSchemes://function?param1=a&param2=b to open the app
//
//
/* *********************
 * HTTP
 * v 1.0
 * *********************
 */
var HTTP = {

    /* ACTIONS */

    // `HTTP.request(param)` send a HTTPRequest
    //
    // `@input: param.type (GET, POST)`
    //
    // `@input: param.format (data, xml, json, text)`
    //
    // `@input: param.timeout (int)`
    //
    // `@input: param.data (Object)`
    //
    // `@callback: param.success (function)`
    //
    // `@callback: param.failure (function)`
    request: function(param) {
        if (Ti.Network.online) {

            var xhr = Ti.Network.createHTTPClient();
            xhr.timeout = param.timeout ? param.timeout : 25000;

            xhr.onload = function(data) {
            	
                if (data) {
                	HTTP.log("debug", "Request.onload > " + JSON.stringify(this, undefined, 2));
                    switch (param.format.toLowerCase()) {
                        case "data":
                            data = this.responseData;
                            break;

                        case "xml":
                            data = this.responseText;
                            break;

                        case "json":
                            data = JSON.parse(this.responseText);
                            break;

                        case "text":
                            data = this.responseText;
                            break;
                    }

                    if (param.success) {
                    	delete xhr;
                        //HTTP.log("info", "Success > " + param.url);
                        if (param.passthrough) {
                            param.success(data, param.url, param.passthrough);
                        } else {
                            param.success(data, param.url);
                        }                        
                    } else {
                    	delete xhr;
                        return data;
                    }
                }
                
            };

            xhr.onerror = function(e) {
                if (param.failure) {
                    if (param.passthrough) {
                        param.failure(this, param.url, param.passthrough);
                    } else {
                        param.failure(this, param.url);
                    }
                } else {
                    Ti.API.error(JSON.stringify(this));
                }

                //HTTP.log("error", "Error > " + JSON.stringify(e));
                delete xhr;
            };



            if (param.ondatastream) {
                xhr.ondatastream = function(e) {
                    if (param.ondatastream) {
                        param.ondatastream(e.progress);
                    }
                };
            }

            param.type = param.type ? param.type : "GET";
            param.async = param.async ? param.async : true;

            xhr.open(param.type, param.url, param.async);

            if (param.headers) {
                for (var i = 0, j = param.headers.length; i < j; i++) {
                    xhr.setRequestHeader(param.headers[i].name, param.headers[i].value);
                }
            }

            /* Overcomes the 'unsupported browser' error sometimes received */
            xhr.setRequestHeader("User-Agent", "Appcelerator Titanium/" + Ti.version + " (" + Ti.Platform.osname + "/" + Ti.Platform.version + "; " + Ti.Platform.name + "; " + Ti.Locale.currentLocale + ";)");

            if (param.data) {
                /* data_encoded = JSON.stringify(param.data); */
                HTTP.log("debug", "Request.data > " + JSON.stringify(param.data, undefined, 2));
                xhr.send(param.data);
               
            } else {
                xhr.send();
            }
        } else {
            HTTP.log("error", "No internet connection");

            if (param.failure) {
                if (param.passthrough) {
                    param.failure(null, param.url, param.passthrough);
                } else {
                    param.failure(null, param.url);
                }
            }
        }
    },



    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[HTTP] " + string);
                break;

            case "debug":
                Titanium.API.debug("[HTTP] " + string);
                break;

            case "error":
                Titanium.API.error("[HTTP] " + string);
                break;
        }
    }

};

/*****************************/

module.exports = HTTP;