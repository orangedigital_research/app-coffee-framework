// NETWORK
// --------------------
// Use this plugin in your app:
//
// `var PUSHNOTIFICATION = require('/plugin/network/pushnotification');`
//
// `exports.register();`
// `exports.subscribe(channel);`

/* *********************
 * PUSH NOTIFICATION
 * v 1.0
 * *********************
 */
var Cloud = require('ti.cloud');

if(OS_ANDROID) var CloudPush = require('ti.cloudpush'); 

exports.getDeviceToken = function() {
    return Titanium.App.Properties.getString('push_devicetoken');
};

exports.setDeviceToken = function(push_devicetoken) {
	Titanium.App.Properties.setString('push_devicetoken', push_devicetoken);
};

// `exports.register()` register the app to receive push nofication
exports.register = function() {
    if(OS_IOS){
    	// After iOS 8 Apple changed the way we register to Push Notifications
    	if (parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
    		// Wait for user settings to be registered before registering for push notifications
		    Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
			    Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush); 
			
			    Ti.Network.registerForPushNotifications({
		            success: function(e) {
	                    exports.setDeviceToken(e.deviceToken);
	                    exports.log('info', "Push Notification registration succeed " + e.deviceToken);
	                  
	                  	/********************** CUSTOM CODE ZONE -> SPECIFIC FOR EACH CLIENT *********************/
	                  	/*****************************************************************************************/
                	},
		            error:  function(e) { exports.log('error', "Push Notification registration failed " + e.error); },
		            callback: function(e) {
	                    exports.log('info', "Push Notification received " + JSON.stringify(e.data));
	       	 			Titanium.UI.iPhone.setAppBadge(Number(Titanium.UI.iPhone.appBadge) + 1);
       	 			
       	 				/********************** CUSTOM CODE ZONE -> SPECIFIC FOR EACH CLIENT *********************/
	                  	/*****************************************************************************************/
               	 	}
		        });
		    });
		    
		    // Register notification types to use
		    Ti.App.iOS.registerUserNotificationSettings({types: [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]});
    	} else {
    		Titanium.Network.registerForPushNotifications({
                success: function(e) {
                    exports.setDeviceToken(e.deviceToken);
                    exports.log('info', "Push Notification registration succeed " + e.deviceToken);
                    
                    /********************** CUSTOM CODE ZONE -> SPECIFIC FOR EACH CLIENT *********************/
	                /*****************************************************************************************/
                },
                error: function(e) { exports.log('error', "Push Notification registration failed " + e.error); },
                callback: function(e) {
                    exports.log('info', "Push Notification received " + JSON.stringify(e.data));
       	 			Titanium.UI.iPhone.setAppBadge(parseInt(Titanium.UI.iPhone.appBadge) + 1);
       	 			
       	 			/********************** CUSTOM CODE ZONE -> SPECIFIC FOR EACH CLIENT *********************/
	                /*****************************************************************************************/
                },
                types: [Titanium.Network.NOTIFICATION_TYPE_BADGE, Titanium.Network.NOTIFICATION_TYPE_ALERT, Titanium.Network.NOTIFICATION_TYPE_SOUND]
            });
    	}
    } else { 
        CloudPush.retrieveDeviceToken({
            success: function(e){
                exports.setDeviceToken(e.deviceToken);
                exports.log('info', "Push Notification registration succeed " + e.deviceToken);
                
                /********************** CUSTOM CODE ZONE -> SPECIFIC FOR EACH CLIENT *********************/
	            /*****************************************************************************************/
                
            },
            error: function(e) { exports.log('error', "Push Notification registration failed " + e.error); }
        });
        
        CloudPush.addEventListener('callback', function (e) {
            notification_data = JSON.parse(e.payload);
            exports.log('info', "Push Notification received " + JSON.stringify(notification_data));
            
            /********************** CUSTOM CODE ZONE -> SPECIFIC FOR EACH CLIENT *********************/
	        /*****************************************************************************************/
        });
    }
    
};


// `exports.unsubscribe()` unsubscribe from a channel
//
// `@param: channel (string)`
exports.unsubscribe = function(channel) {
    Cloud.PushNotifications.unsubscribeToken({
        device_token: exports.getDeviceToken(),
        channel: channel,
    }, function(e) {
        if (e.success) {
            exports.log("info", "Unsubcribe from: " + JSON.stringify(channels));
        } else {
            exports.log("error", "Unsubcription not found " + ((e.error && e.message) || JSON.stringify(e)));
        }
    });
};

// `exports.subcribeBulk()` subscribe to multiples channels
//
// `@param: channel (array)`
exports.subcribeBulk = function(channels) {
    for (pn = 0; pn < channels.length; pn++) {
        exports.subscribe(channels[pn]);
    }
};

// `exports.subscribe()` subscribe to a channels
//
// `@param: channel (string)`
exports.subscribe = function(channel) {
    Cloud.PushNotifications.subscribeToken({
        type: Ti.Platform.name == 'android' ? 'android' : 'ios',
        channel: channel,
        device_token: exports.getDeviceToken()
    }, function(e) {
        if (e.success) {
            exports.log("info", "Subscribe to " + channel);
        } else {
            exports.log("error", "Subscription failed for " + channel + " - " + ((e.error && e.message) || JSON.stringify(e)));
        }
    });
};

exports.resetBadge = function(){
   if (OS_IOS) Titanium.UI.iPhone.setAppBadge(null);

   Cloud.PushNotifications.setBadge({
	    device_token: exports.getDeviceToken(),
	    badge_number: '0'
   }, function (e) {
      if (e.success) {
	        exports.log("info", "Badge Set!");
	    };
   });
};
    
exports.isNotificationsEnabled = function() {
	if (OS_IOS) {
    	if (Titanium.App.iOS.getCurrentUserNotificationSettings().types[0]) {
    		return true;
    	} else {
    		return false;
    	}
	} else {
		return true;
	}
};

exports.log = function(type, string) {
    switch (type) {
        case "info":
            Titanium.API.info("[PUSH NOTIFICATION] " + string);
            break;
        case "debug":
            Titanium.API.debug("[PUSH NOTIFICATION] " + string);
            break;
        case "error":
            Titanium.API.error("[PUSH NOTIFICATION] " + string);
            break;
    }
};