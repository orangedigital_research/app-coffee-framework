// NETWORK
// --------------------
// Use this plugin in your app:
//
// `var EMAIL = require('/plugin/network/email');`
//
// `EMAIL.init();`
//
// `EMAIL.compose(param);`

/* *********************
 * EMAIL PLUGIN
 * v 1.0
 * *********************
 */
var emailTest = Ti.UI.createEmailDialog();
var EMAIL = {

    enabled: emailTest.isSupported(),

    /* INIT */

    init: function() {
        var emailTest = Ti.UI.createEmailDialog();
        EMAIL.enabled = emailTest.isSupported();
        emailTest = null;
    },

    /* ACTION */

    // `EMAIL.compose(param)` compose a new email
    //
    // `@input: param.subject (string)`
    //
    // `@input: param.body (string)`
    //
    // `@input: param.emails (array)`
    //
    // `@input: param.attachment (blob)`
    compose: function(param) {
        if (EMAIL.enabled) {

            var emailDialog = Ti.UI.createEmailDialog({
                html: true,
                subject: param.subject,
                messageBody: param.body
            });

            if (param.emails != null) {
                EMAIL.log('info', 'compose > ' + JSON.stringify(param.emails));
                emailDialog.setToRecipients(param.emails);
            }

            if (param.attachment != null) {
                emailDialog.addAttachment(param.attachment);
            }

            emailDialog.open();

        } else {
            var noemail_alt = Ti.UI.createAlertDialog({
                message: 'No email account has been set up. Please, check your email settings.',
                ok: 'OK',
                title: 'Notice'
            }).show();
            noemail_alt = null;
        }

    },

    // `EMAIL.pick(param)` pick an email from a list and compose a new email
    //
    // `@input: param (Object)`
    //
    // `@callback: param.cancel (function)`
    pick: function(param) {
        if (param.emails.length == 0) {
            EMAIL.alertNoEmail();
        } else if (param.emails.length == 1) {
            /* Only 1 email */
            EMAIL.compose(param);
        } else {
            /* Multiple emails */
            email_choice = param.emails;
            email_choice.push('Cancel');
            var cancel_pos = param.emails.length - 1;
            var optionEmails = {
                options: email_choice,
                cancel: cancel_pos
            };
            var optionInviteEmailDialog = Titanium.UI.createOptionDialog(optionEmails);
            optionInviteEmailDialog.addEventListener('click', function(e) {
                switch (e.index) {
                    default:
                    /* Email */
                    param.emails = [email_choice[e.index]];
                    EMAIL.compose(param);
                    break;

                    case cancel_pos:
                        /* Cancel */
                    if (typeof param.cancel != "undefined") {
                        param.cancel();
                    }
                    break;
                }
            });

            optionInviteEmailDialog.show();
        }

    },

    // `EMAIL.validate(email)` validate email syntax
    //
    // `@input: email (String)`
    //
    // `@output: Boolean`
    validate: function(email){
        var validateEmail = /([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/g;
        return validateEmail.test(email);
    },

    /* ALERT */

    alertNoEmail: function() {
        var network_alt = Ti.UI.createAlertDialog({
            message: 'No email address provided',
            ok: 'OK',
            title: 'Notice'
        }).show();
        network_alt = null;
    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[EMAIL] " + string);
                break;

            case "debug":
                Titanium.API.debug("[EMAIL] " + string);
                break;

            case "error":
                Titanium.API.error("[EMAIL] " + string);
                break;
        }
    },


};

/*****************************/

module.exports = EMAIL;