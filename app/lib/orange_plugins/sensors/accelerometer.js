// SENSORS
// --------------------
// Use this plugin in your app:
//
// `var ACCELEROMETER = require('/plugin/sensors/accelerometer');`
//
// `ACCELEROMETER.int(callback);`

/* *********************
 * ACCELEROMETER
 * v 1.0
 * *********************
 */
var ACCELEROMETER = {

    value: {
        x: 0,
        y: 0,
        z: 0
    },

    /* INIT */

    // Initialise plugin and set up callback `ACCELEROMETER.init(callback)`
    //
    // `@return: {x, y, z}`
    //
    // `@callback: callback (function)`   
    init: function(callback) {

        function eventAccelerometerUpdate(e) {
            ACCELEROMETER.value.x = e.x;
            ACCELEROMETER.value.y = e.y;
            ACCELEROMETER.value.z = e.z;

            callback(ACCELEROMETER.value);
        }

        if (OS_ANDROID) {

            Ti.Android.currentActivity.addEventListener('pause', function(e) {
                Titanium.Accelerometer.removeEventListener('update', eventAccelerometerUpdate);
            });

            Ti.Android.currentActivity.addEventListener('resume', function(e) {
                Titanium.Accelerometer.addEventListener('update', eventAccelerometerUpdate);
            });

            Titanium.Accelerometer.addEventListener('update', eventAccelerometerUpdate);
        }

        if (OS_IOS) {
            Titanium.Accelerometer.addEventListener('update', eventAccelerometerUpdate);
        }

    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[ACCELEROMETER] " + string);
                break;

            case "debug":
                Titanium.API.debug("[ACCELEROMETER] " + string);
                break;

            case "error":
                Titanium.API.error("[ACCELEROMETER] " + string);
                break;
        }
    },

};

/*****************************/

module.exports = ACCELEROMETER;