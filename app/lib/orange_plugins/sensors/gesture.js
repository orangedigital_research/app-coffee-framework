// SENSORS
// --------------------
// Use this plugin in your app:
//
// `var GESTURE = require('/plugin/sensors/gesture');`
//
// `GESTURE.int(callback);`

/* *********************
 * GESTURE PLUGIN
 * v 1.0
 * *********************
 */
var GESTURE = {

    api: Titanium.Gesture,

    value: {
        gx: 0,
        gy: 0,
        gz: 0,
        orientation: null
    },

    /* INIT */

    // Initialise plugin and set up callback `GESTURE.init(callback)`
    //
    // `@return: {gx, gy, gz, orientation}`
    //
    // `@callback: callback (function)`  
    init: function(callback) {
        GESTURE.value.orientation = GESTURE.api.orientation;
        GESTURE.callback = callback;
    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[ GESTURE ] " + string);
                break;

            case "debug":
                Titanium.API.debug("[ GESTURE ] " + string);
                break;

            case "error":
                Titanium.API.error("[ GESTURE ] " + string);
                break;
        }
    },

    /* OBSERVER */

    observerGestureShake: function(e) {
        GESTURE.log("info", "gesture:observerGestureShake");
        GESTURE.value.gx = e.x;
        GESTURE.value.gy = e.y;
        GESTURE.value.gz = e.z;

        GESTURE.callback(GESTURE.value);
    },

    observerGestureOrientation: function(e) {
        GESTURE.log("info", "gesture:observerGestureOrientation");
        GESTURE.value.orientation = e.orientation;
        GESTURE.callback(GESTURE.value);
    }

};

/*****************************/

function eventGestureShake(e) {
    GESTURE.observerGestureShake(e);
}
GESTURE.api.addEventListener('shake', eventGestureShake);

function eventGestureOrientation(e) {
    GESTURE.observerGestureOrientation(e);
}
GESTURE.api.addEventListener('orientationchange', eventGestureOrientation);

/*****************************/

module.exports = GESTURE;