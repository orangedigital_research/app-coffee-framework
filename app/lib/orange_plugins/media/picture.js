/**
 * Provides several helper methods to interact with the phone's camera. <br>
 * Please not that this module requires the ti.imagefactory module imported to the project!
 *
 * @module Picture
 */


/**
 * Prompts the user to select a photo of his gallery.
 *  From iOS 10 and early systems you need to set a permission key in your tiapp.xml
 *  <key>NSPhotoLibraryUsageDescription</key>
 *  <string>Can we open your photo library?</string>
 *
 * @param {Function} param.success - Function to be executed after the user picks an image from the gallery 
 * @param {Function} param.cancel - Function to be executed if the user cancels the image picking
 * @param {Function} param.error - Function to be executed if some error happens while picking an image
 * @param {Function} param.mediaTypes - Function to be executed after the user picks a photo from the gallery
 */
exports.pick = function(param) {
    Titanium.Media.openPhotoGallery({
        success: function(event) {
            param.success(event.media);
        },
        cancel: function(event) {
            param.cancel(event);
        },
        error: function(event) {
            param.error(event);
        },
        allowEditing: true,
        mediaTypes: param.mediaTypes || [Ti.Media.MEDIA_TYPE_VIDEO, Ti.Media.MEDIA_TYPE_PHOTO]
    });
};

/**
 * Saves a blob to the user's gallery
 *
 * @param {Blob} param.image - Blob to be saved
 * @param {Function} param.success - Function to be executed after the user picks an image from the gallery
 * @param {Function} param.cancel - Function to be executed if the user cancels the image picking
 * @param {Function} param.error - Function to be executed if some error happens while picking an image
 */
exports.save = function(param) {
    Titanium.Media.saveToPhotoGallery(param.image, {
        success: param.success,
        cancel: param.cancel,
        error: param.error
    });
};

/**
 * Takes a screenshot and saves it to the gallery
 *
 * @param {Ti.UI.Whatever} view - The view to take the screenshot
 */
exports.screenshot = function(view) {
    var screenshot = view.toImage();
    var param = {
        image: screenshot,
        success: function() {
        },
        cancel: function() {},
        error: function() {},
    };

    PICTURE.save(param);
    screenshot = null;
};

/**
 * Resized an image based on a percentage
 *
 * @param {Object} blob - The image you want to resize
 * @param {Object} percentage - The percentage you want your image resized
 */
exports.resizeKeepAspectRatioPercentage = function(blob, percentage) {
	if (blob.width == 0 || blob.height == 0 || percentage == 0)
		return blob;

	var w = blob.width * (percentage / 100);
	var h = blob.height * (percentage / 100);

	return blob.imageAsResized(w, h);
};
