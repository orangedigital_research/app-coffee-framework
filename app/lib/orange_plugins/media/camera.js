/**
 * Provides several helper methods to interact with the cellphone camera.
 *
 * @module Camera
 */

var current = Titanium.Media.CAMERA_REAR;

/**
 *	Shows the camera overlay
 *  From iOS 10 and early systems you need to set a permission key in your tiapp.xml
 *  <key>NSCameraUsageDescription</key>
 *  <string>Can we use your camera?</string>
 *
 * @param {Dictionary<CameraOptionsType>} param - Check http://docs.appcelerator.com/platform/latest/#!/api/CameraOptionsType
 */
exports.show = function(param) {
	if (Ti.Media.hasCameraPermissions()){
		Titanium.Media.showCamera({
            success: function(event) {
                param.success && param.success(event);
            },
            cancel: function(event) {
                param.cancel && param.cancel(event);
            },
            error: function(event) {
                param.error && param.error(event);
            },
            animated: true,
            showControls: param.showControls,
            videoQuality: param.videoQuality,
            videoMaximumDuration: param.videoMaximumDuration,
            saveToPhotoGallery: param.saveToPhotoGallery,
            allowEditing: param.allowEditing,
            mediaTypes: param.mediaTypes
        });
	} else {
		CAMERA.requestAccess(param.requestAccessCallback);
	}
};

/** Opens the native Camera settings */
exports.editPermissions = function() {
	if (OS_IOS) {
		Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
	}

	if (OS_ANDROID) {
		var intent = Ti.Android.createIntent({
			action: 'android.settings.APPLICATION_SETTINGS',
		});
		intent.addFlags(Ti.Android.FLAG_ACTIVITY_NEW_TASK);
		Ti.Android.currentActivity.startActivity(intent);
	}
};

/** Requests the user for permission to use the Camera */
exports.requestAccess = function(callback){
	// On iOS we can get information on the reason why we might not have permission
	if (OS_IOS) {
		if (Ti.Media.cameraAuthorizationStatus === Ti.Media.CAMERA_AUTHORIZATION_DENIED) {
    		CAMERA.editPermissions();
    		return;
    	}
	}

	Ti.Media.requestCameraPermissions(function(e) {
		if (e.success) {
			callback({'status':'success', 'message': 'Camera permission granted successfully.'});
		} else if (OS_ANDROID) {
			callback({'status':'failure', 'message': "You don't have the required uses-permissions in tiapp.xml or you denied permission for now, forever or the dialog did not show at all because you denied forever before."});
		} else {
			callback({'status':'failure', 'message': "Camera permission not granted successfully. User has denied permission."});
		}
	});
};

/** Closes the camera overlay*/
exports.hide = function() {
    Titanium.Media.hideCamera();
};

/** Takes a picture */
exports.take = function() {
    Titanium.Media.takePicture();
};

/** Switches between the front/back cameras */
exports.switchCamera = function() {
    if (current == Titanium.Media.CAMERA_REAR) {
        Titanium.Media.switchCamera(Titanium.Media.CAMERA_FRONT);
    } else {
        Titanium.Media.switchCamera(Titanium.Media.CAMERA_REAR);
    }
};
