/**
 * Provides easy access to Google Analytics API.
 * @module ti.ga
 */

var ga = require('ti.ga');
var tracker;

/**
 * Initialise plugin and set up the Google Analytics AppID
 * 
 * @param {Integer} id - Application ID set on Google Analytics panel
 * @param {Boolean} secure 
 * @param {Boolean} debug 
 */
exports.init = function(id, secure, debug) {
	ga.setDispatchInterval(30);
	ga.setTrackUncaughtExceptions();
	
	tracker = ga.createTracker({
		trackingId: id,
		useSecure: secure || true,
		debug: debug || false
	});
};

exports.startSession = function() {
	tracker.startSession();
};

exports.endSession = function() {
	tracker.endSession();
	
	ga.dispatch();
};

/**
 * The dispatch method submits data to Google Analytics. Google Analytics will automatically do this for your, 
 * but you can "force" this programmatically. If you are going to use this it should only be done while your application 
 * is not active and has a network connection.
 */
exports.dispatch = function() {
	ga.dispatch();
};

/**
 * Track an event
 * 
 * @param {String} category
 * @param {String} action 
 * @param {String} label
 * @param {String} value
 */
exports.trackEvent = function(category, action, label, value) {
	tracker.addEvent({
		category: category,
		action: action,
		label: label || '', // Optional
		value: value || 0 // Optional
	});
};

/**
 * Track a view
 * 
 * @param {String} screen - view to track
 */
exports.trackScreen = function(screen) {
	tracker.addScreenView(screen);
}; 

/**
 * Track an social event
 * 
 * @param {String} network
 * @param {String} action
 * @param {String} target
 * 
 */
exports.trackSocial = function(network, action, target) {
	tracker.addSocialNetwork({
        network: network,
		action: action,
		target: target
    });  
};

exports.trackException = function(description, fatal) {
	tracker.addException({
        description: description,
        fatal: fatal || false
    });  
};

/* LOG */
exports.log = function(type, string){
	
	switch (type) {
        case "info":
            Titanium.API.info("[GANALYTICS] " + string);
            break;

        case "debug":
            Titanium.API.debug("[GANALYTICS] " + string);
            break;

        case "error":
            Titanium.API.error("[GANALYTICS] " + string);
            break;
    }
	
};
