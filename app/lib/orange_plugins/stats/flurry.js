// SYSTEM
// --------------------
// Use this plugin in your app:
//
// `var FLURRY = require('/plugin/stats/flurry');`
//
// `FLURRY.track(param);`

/* *********************
 * FLURRY
 * v 1.0
 * *********************
 */
var FLURRY = {

    // `FLURRY.api.*` links to the ti.Flurry API
    api: require('ti.flurry'),

    /* INIT */

    // Initialise plugin and set up the Flurry AppID `FLURRY.init(app_id)`
    //
    // `@input: app_id (int)`    
    init: function(app_id) {      
        
        if(OS_IOS){
            FLURRY.api.sessionReportsOnPauseEnabled = true;
            FLURRY.api.reportOnClose = true;
            FLURRY.api.eventLoggingEnabled = false;
            FLURRY.api.debugLogEnabled = false;
            FLURRY.api.secureTransportEnabled = true;
        }
        
        FLURRY.api.initialize(app_id);
    },

    /* ACTIONS */

    // `FLURRY.track(param)` track an event
    //
    // `@input: param.event_name (string)`
    //
    // `@input: param.event_parameter (Object)`
    track: function(param) {
        FLURRY.api.logEvent(param.event_name, {data: param.event_parameters});
    },

    // `FLURRY.track(param)` track an event in time
    //
    // `@input: param.event_name (string)`
    //
    // `@input: param.start (Boolean)`
    //
    // `@input: param.event_parameters (Object)`
    //
    // `@input: param.start (Boolean)`
    track_time: function(param) {
        if (OS_IOS) {
            if (param.start) {
                FLURRY.api.logTimedEvent(param.event_name, {data: param.event_parameters});
            } else {
                FLURRY.api.endTimedEvent(param.event_name, {data: param.event_parameters});
            }
        }
    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[FLURRY] " + string);
                break;

            case "debug":
                Titanium.API.debug("[FLURRY] " + string);
                break;

            case "error":
                Titanium.API.error("[FLURRY] " + string);
                break;
        }
    }
};

/*****************************/

module.exports = FLURRY;