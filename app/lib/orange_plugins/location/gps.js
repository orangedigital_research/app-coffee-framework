/**
 * Provides several helper methods to interact with the phone GPS<br/><br/>
 * 
 * ---------  iOS implementation notes -----------<br/>
 * On TiApp.xml, inside the iOS plist dictionary, you should add a text which will be shown to the user when requesting access to the gps.<br/><br/>
 * 
 * Only when app is on the foreground:<br/>
 * <key>NSLocationWhenInUseUsageDescription</key><br/>
 * <string> We would like to access your location in order to ... </string><br/><br/>
 * 
 * With the app on the foreground / background:<br/>
 * <key>NSLocationAlwaysUsageDescription</key><br/>
 * <string> We would like to access your location in order to ... </string><br/> 
 * ---------------------
 * 
 * 
 * @module Gps
 */

/** Local reference to the Ti SDK geolocation module */
var API = Titanium.Geolocation;

/** Local reference to a Location Provider (ANDROID). This is used for constant movement tracking*/
var location_provider;


/**
 * Initialize the module and requests access to the GPS
 *  
 * @param {Integer} param.authorization_type - The type of authorization to ask access to
 */
exports.init = function(param) {
    if ((OS_IOS && param.authorization_type) || OS_ANDROID) {
    	// The new cross-platform way to check permissions
		if (API.hasLocationPermissions(param.authorization_type || null) == false) {
        	exports.requestAccess(param.authorization_type);
        }
    }
    
    if (OS_ANDROID) {
		location_provider = API.Android.createLocationProvider({
			name : API.PROVIDER_GPS,
			minUpdateTime : 60,
			minUpdateDistance : 150
		});
	}
};

/** Opens the native GPS settings */
exports.editPermissions = function() {
	if (OS_IOS) {
		Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
	}

	if (OS_ANDROID) {
		var intent = Ti.Android.createIntent({
			action: 'android.settings.APPLICATION_SETTINGS',
		});
		
		intent.addFlags(Ti.Android.FLAG_ACTIVITY_NEW_TASK);
		
		Ti.Android.currentActivity.startActivity(intent);
	}
};

/** 
 * Requests the user for permission to use the Calendar 
 *
 * @param {Integer} authorization_type - The type of authorization to ask access to 
 */
exports.requestAccess = function(authorization_type){
	// On iOS we can get information on the reason why we might not have permission
	if (OS_IOS) {
		if (API.locationServicesAuthorization == API.AUTHORIZATION_DENIED) {
    		exports.editPermissions();
    		return;
    	}
	}

	Ti.Geolocation.requestLocationPermissions(authorization_type, function(e) {
		if (e.success) {
			Ti.API.info("Location permission granted successfully.");
		} else if (OS_ANDROID) {
			Ti.API.error("You don't have the required uses-permissions in tiapp.xml or you denied permission for now, forever or the dialog did not show at all because you denied forever before.");
		} else {
			Ti.API.error("Location permission not granted successfully. User has denied permission.");
		}
	});
};

/**
 * Returns the current user location
 * 
 * @param {Function} callback - The function to execute if the user coords have been found.
 */
exports.getPosition = function(callback) {
	if (API.locationServicesEnabled) {
        if (OS_ANDROID) API.purpose = "Get Current Location";
        
        API.accuracy = API.ACCURACY_HIGH;
        
		API.getCurrentPosition(function(e) {
			if (e.success && e.coords) {
				callback && callback(e.coords);
			} else {
				callback && callback(null);
			}
		});
    } else {
    	exports.alertDisable();
    }
};


/**
 * Tracks the user location based on a set of rules.
 * 
 * ----- iOS ------
 * You must set the API.accuracy and API.distanceFilter according to your needs
 * 
 * ---- Android ----
 * You must set a Location Provider to the module. One is already being created in the INIT method of this module.
 * 
 * @param {Function} callback - The function to be executed whenever the user position changes.
 */
eexports.trackUserPosition = function(callback) {
	if (API.locationServicesEnabled) {
		if (OS_IOS) {
			API.accuracy = API.ACCURACY_HIGH;
			API.distanceFilter = 150;
		}

		if (OS_ANDROID) {
			API.Android.manualMode = true;
			API.Android.addLocationProvider(location_provider);
		}
		
		Titanium.Geolocation.addEventListener('location', function() {
			if (e.success && e.coords) {
				callback && callback(e.coords);
			}
		});
		
	}
};


/**
 * Returns the address of the user based on his current location.
 * 
 * @param {Function} callback - The function to be executed after the reversed user location has been found.
 */
exports.getReverseGeocoder = function(callback) {
    exports.getPosition(function(location) {
    	if (location.success && location.coords) {
	        API.reverseGeocoder(e.coords.latitude, e.coords.longitude, function(e) {
	            if (e.success) {
					callback && callback(e.places);
	                return;
	            }
	            
            	callback && callback(null);
	
	        });
    	}
    });
};


/**
 * Tries to find a map coordinate based on the address that the user provides.
 * 
 * @param {String} address - A String containing a valid address 
 * @param {Function} callback - The function to be executed if a coordinate is successfully found
 */
exports.getGeocoder = function(address, callback) {
    var HTTP = require('orange_plugins/network/http');
    
    var param = {
        url: "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" + address,
        format: "json",
        success: function(data, url) {
            callback && callback({latitude: data.results[0].geometry.location.lat, longitude: data.results[0].geometry.location.lng});
        },
        failure: function() {
        	callback && callback({latitude: 0, longitude: 0});
        }
    };

    HTTP.request(param);
};

/** Alerts the user if his GPS is disable */
exports.alertDisable = function() {
    Ti.UI.createAlertDialog({
        message: 'Seems like your GPS is disabled. Please check your phone location settings.',
        ok: 'OK',
        title: 'Oops'
    }).show();
};