/**
 * Provides a few helper methods to interact with the native Map module.
 * 
 * @module Map
 */


/** Local reference to the Map module */ 
var API = require('ti.map');


/**
 * Converts a point (x,y) clicked in your map to a coordinate
 * 
 * @param {Modules.Map.View} map - A Map View
 * @param {Integer} x - The X point of the place in the screen you've clicked
 * @param {Integer} y - The Y point of the place in the screen you've clicked
 */
exports.convertPixeltoLatLng = function(map, x, y) {
    var heightDegPerPixel = -map.region.latitudeDelta / map.rect.height;
    var widthDegPerPixel = map.region.longitudeDelta / map.rect.width;

    return {
        latitude: (y - map.rect.height / 2) * heightDegPerPixel + map.region.latitude,
        longitude: (x - map.rect.width / 2) * widthDegPerPixel + map.region.longitude
    };
};

/**
 * Returns a region and zoom which fits in the map view all the points passed as parameter. 
 * 
 * @param {Dictionary} points - An Array with {latitude: <>, longitude: <>} objects.
 */
exports.findZoomRegion = function(points) {
	var nbPtToShow = points.length - 1;
	var tmpDeltatLat = 0,
	    tmpDeltatLong = 0,
	    maxDeltatLat = 0,
	    maxDeltatLong = 0,
	    centerLat = 0,
	    centerLong = 0;

	for (var i = 0; i <= Math.floor(points.length / 2); i++) {
		for (var j = nbPtToShow; j >= Math.floor(points.length / 2); j--) {
			if (j != i) {
				tmpDeltatLat = Math.abs(Math.abs(points[i].latitude) - Math.abs(points[j].latitude));
				
				if (tmpDeltatLat > maxDeltatLat) {
					maxDeltatLat = tmpDeltatLat;
					centerLat = Math.min(points[i].latitude, points[j].latitude) + maxDeltatLat / 2;
				}
				
				tmpDeltatLong = Math.abs(Math.abs(points[i].longitude) - Math.abs(points[j].longitude));
				
				if (tmpDeltatLong > maxDeltatLong) {
					maxDeltatLong = tmpDeltatLong;
					centerLong = Math.min(points[i].longitude, points[j].longitude) + maxDeltatLong / 2;
				}
			}
		}
	}
	
	return {
		latitude : centerLat,
		longitude : centerLong,
		latitudeDelta : maxDeltatLat * 2,
		longitudeDelta : maxDeltatLong * 2,
	};
};

/**
 * This method returns the distance in meters between two points 
 * 
 * @param {Float} lat1 - The origin latitude
 * @param {Float} lon1 - The origin longitude
 * @param {Float} lat2 - The destination latitude
 * @param {Float} lon2 - The destination longitude
 */
exports.getDistanceFromLatLonInMeters = function(lat1,lon1,lat2,lon2) {
	var R = 6371; // Radius of the earth in km
    var dLat = Math.toRadians(lat2-lat1); 
    var dLon = Math.toRadians(lon2-lon1);
    
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
 	var d = R * c * 1000;
 	
  	return Math.ceil(d);
};
/**
 * This method open a option dialog and let the user set the map terrain mode 
 * 
 * @param {Modules.Map.View} map - A Map View
 * 
 */
exports.pickType = function(map) {
    var optionMapTypeChoice = {
        options: ['Normal', 'Satellite', 'Hybrid', 'Cancel'],
        cancel: 3,
    };
    var optionMapType = Titanium.UI.createOptionDialog(optionMapTypeChoice);
    optionMapType.addEventListener('click', function(e) {
        switch (e.index) {
            case 0:
				map.setMapType(exports.api.NORMAL_TYPE);
                break;

            case 1:
            	map.setMapType(exports.api.SATELLITE_TYPE);
                break;

            case 2:
                map.setMapType(exports.api.HYBRID_TYPE);
                break;

            case 3: //Cancel
                break;
        }
    });

    optionMapType.show();
};