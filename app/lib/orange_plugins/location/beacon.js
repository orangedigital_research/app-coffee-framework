// LOCATION
// --------------------
// Use this plugin in your app:
//
// `var BEACON = require('/plugin/location/beacon');`
//
// `BEACON.init(param);`
//
// `BEACON.startScanner(param);`

/* *********************
 * BEACON
 * v 1.0 - iOS 7 Only
 * *********************
 */
var BEACON = {

    api: null,

    callback: {
        advertise: null,
        enteredRegion: null,
        exitedRegion: null,
        determinedRegionState: null,
        ranges: null,
        proximity: null
    },


    /* INIT */

    // Initialise plugin and set up callback `BEACON.init(param)`
    //
    // `@callback: param.advertise (function)`
    //
    // `@callback: param.enteredRegion (function)`
    //
    // `@callback: param.exitedRegion (function)`
    //
    // `@callback: param.determinedRegionState (function)`
    //
    // `@callback: param.ranges (function)`
    //
    // `@callback: param.proximity (function)`
    init: function(param) {
        if (OS_IOS) {
            BEACON.api = require('org.beuckman.tibeacons');
            BEACON.callback.advertise = param.advertise;
            BEACON.callback.enteredRegion = param.enteredRegion;
            BEACON.callback.exitedRegion = param.exitedRegion;
            BEACON.callback.determinedRegionState = param.determinedRegionState;
            BEACON.callback.ranges = param.ranges;
            BEACON.callback.proximity = param.proximity;
            
            /*****************************/
            
            function eventBeaconAdvertisingStatus(e) {
                BEACON.observerBeaconAdvertisingStatus(e);
            }
            BEACON.api.addEventListener('advertisingStatus', eventBeaconAdvertisingStatus);
            
            function eventBeaconEnteredRegion(e) {
                BEACON.observerBeaconEnteredRegion(e);
            }
            BEACON.api.addEventListener('enteredRegion', eventBeaconEnteredRegion);
            
            function eventBeaconExitedRegion(e) {
                BEACON.observerBeaconExitedRegion(e);
            }
            BEACON.api.addEventListener('exitedRegion', eventBeaconExitedRegion);
            
            function eventBeaconDeterminedRegionState(e) {
                BEACON.observerBeaconDeterminedRegionState(e);
            };
            BEACON.api.addEventListener('determinedRegionState', eventBeaconDeterminedRegionState);
            
            function eventBeaconRanges(e) {
                BEACON.observerBeaconRanges(e);
            }
            BEACON.api.addEventListener('beaconRanges', eventBeaconRanges);
            
            function eventBeaconProximity(e) {
                BEACON.observerBeaconProximity(e);
            }
            BEACON.api.addEventListener('beaconProximity', eventBeaconProximity);
            
            /*****************************/
            
            if (param.autoRanging) {
                BEACON.api.enableAutoRanging();
            }
           
            
        } else {
            BEACON.log("error", "init > No beacon module for this device");
        }

    },

    getBeacon: function() {
        var beacon = {
            uuid: Titanium.App.Properties.getString('beacon_uuid'),
            identifier: Titanium.App.Properties.getString('beacon_identifier'),
            major: Math.abs(parseInt(Titanium.App.Properties.getString('beacon_major'))),
            minor: Math.abs(parseInt(Titanium.App.Properties.getString('beacon_minor')))
        };
        return beacon;
    },

    getBeaconIdentifier: function(param) {
        var beacon_identifier = Titanium.App.Properties.getString(param.uuid + "_" + param.major + "_" + param.minor);
        return beacon_identifier;
    },

    setBeacon: function(param) {
        Titanium.App.Properties.setString(param.uuid + "_" + param.major + "_" + param.minor, param.identifier);
        Titanium.App.Properties.setString('beacon_uuid', param.uuid);
        Titanium.App.Properties.setString('beacon_identifier', param.identifier);
        Titanium.App.Properties.setString('beacon_major', param.major);
        Titanium.App.Properties.setString('beacon_minor', param.minor);
    },


    /* ACTION */

    // `BEACON.startEmiter(param)` start device acting like a beacon
    //
    // `@input: param.uuid (string)`
    //
    // `@input: param.identifier (string)`
    //
    // `@input: param.major (int)`
    //
    // `@input: param.minor (int)`
    startEmiter: function(param) {
        BEACON.log("info", "start > " + JSON.stringify(param));
        BEACON.setBeacon(param);
        BEACON.api.startAdvertisingBeacon({
            uuid: param.uuid,
            identifier: param.identifier,
            major: Math.abs(parseInt(param.major)),
            minor: Math.abs(parseInt(param.minor))
        });
    },

    // `BEACON.stopEmiter(param)` stop device acting like a beacon
    stopEmiter: function(param) {
        BEACON.log("info", "stop > " + JSON.stringify(param));
        BEACON.api.stopAdvertisingBeacon();
    },

    startMonitor: function(param) {
        BEACON.log("info", "startMonitor > " + JSON.stringify(param));
        BEACON.api.startMonitoringForRegion({
            uuid: param.uuid,
            identifier: param.identifier,
            major: Math.abs(parseInt(param.major)),
            minor: Math.abs(parseInt(param.minor))
        });
    },

    stopMonitor: function(param) {
        BEACON.log("info", "stopMonitor > " + JSON.stringify(param));
        BEACON.api.stopMonitoringAllRegions();
    },

    // `BEACON.startScanner(param)` start device scanner for a specific beacon
    //
    // `@input: param.uuid (string)`
    //
    // `@input: param.identifier (string)`
    //
    // `@input: param.major (int)`
    //
    // `@input: param.minor (int)`    
    startScanner: function(param) {
        BEACON.log("info", "startScan > " + JSON.stringify(param));
        BEACON.api.startRangingForBeacons({
            uuid: param.uuid,
            identifier: param.identifier,
            major: Math.abs(parseInt(param.major)),
            minor: Math.abs(parseInt(param.minor))
        });
    },

    // `BEACON.stopScanner()` stop device scanner
    stopScanner: function() {
        BEACON.log("info", "stopScan");
        BEACON.api.stopRangingForAllBeacons();
    },


    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[BEACON] " + string);
                break;

            case "debug":
                Titanium.API.debug("[BEACON] " + string);
                break;

            case "error":
                Titanium.API.error("[BEACON] " + string);
                break;
        }
    },


    /* LOG */

    observerBeaconAdvertisingStatus: function(e) {
        /* BEACON.log("info","beacon:observerAdvertisingStatus > "+JSON.stringify(e)); */
        BEACON.callback.advertise(e);
    },

    observerBeaconEnteredRegion: function(e) {
        /* BEACON.log("info","beacon:observerEnteredRegion > "+JSON.stringify(e)); */
        BEACON.callback.enteredRegion(e);
    },

    observerBeaconExitedRegion: function(e) {
        /* BEACON.log("info","beacon:observerExitedRegion > "+JSON.stringify(e)); */
        BEACON.callback.exitedRegion(e);
    },

    observerBeaconDeterminedRegionState: function(e) {
        /* BEACON.log("info","beacon:observerDeterminedRegionState > "+JSON.stringify(e)); */
        BEACON.callback.determinedRegionState(e);
    },

    observerBeaconRanges: function(e) {
        /* BEACON.log("info","beacon:eventBeaconRanges > "+JSON.stringify(e)); */
        BEACON.callback.ranges(e);
    },

    observerBeaconProximity: function(e) {
        /* BEACON.log("info","beacon:observerBeaconProximity > "+JSON.stringify(e)); */
        BEACON.callback.proximity(e);
    },

};

/*****************************/

module.exports = BEACON;