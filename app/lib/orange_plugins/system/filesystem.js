/**
 * Provides several helper methods to interact with the Ti.Filesystem module.
 * @module Filesystem
 */

/** Local reference to the App's data directory. */
var app_data_directory = Ti.Filesystem.applicationDataDirectory;

/**
 * Checks if a file exists.
 * 
 * @param {String} path - The path to a certain file
 * @return {Boolean} - Whether the file/directory exists 
 */
exports.checkExists = function(path) {
	return Ti.Filesystem.getFile(FILESYSTEM.app_data_directory + path).exists();
};

/**
 * Moves a certain file to a new path.
 * 
 * @param {Object} params - Object containing the options to use with this method
 * @param {String} params.path - The path to the file you wish to move
 * @param {String} params.new_path - The new path
 * @param {Function} [params.success] - Function to execute in case the file is successfully moved
 * @param {Function} [params.error] - Function to execute in case the file is not moved
 */
exports.move = function(params) {
    if (Ti.Filesystem.getFile(app_data_directory + params.path).move(app_data_directory + params.new_path)) {
    	params.success && params.success();
    } else {
    	params.error && params.error();
    }
};

/**
 * Renames a certain file.
 * 
 * @param {Object} params - Object containing the options to use with this method
 * @param {String} params.path - The path to the file you wish to rename
 * @param {String} params.new_name - The new name of the file
 * @param {Function} [params.success] - Function to execute in case the file is successfully moved
 * @param {Function} [params.error] - Function to execute in case the file is not moved
 */
exports.rename = function(params) {
    if (Ti.Filesystem.getFile(app_data_directory + params.path).rename(params.new_name)) {
    	params.success && params.success();
    } else {
    	params.error && params.error();
    }
};

/**
 * Saves or append content to a certain file.
 * 
 * @param {Object} params - Object containing the options to use with this method
 * @param {String} params.path - The name of the file to be saved 
 * @param {(String|Ti.Filesystem.File|Ti.Blob)} params.content - The content to write on this file
 * @param {Boolean} [params.append=false] - Controls whether you want to overwrite an existing file with the passed name or append content to it
 * @param {Function} [params.success] - Function to execute in case the file is successfully saved
 * @param {Function} [params.error] - Function to execute in case the file is not saved
 */
exports.saveFile = function(params) {
	if (Ti.Filesystem.getFile(app_data_directory + params.path).write(params.content, params.append || false)) {
		params.success && params.success();
	} else {
        params.error && params.error();
    }
};

/**
 * Returns a Ti.Filesytem.File object 
 * 
 * @param {String} path - The path to a file 
 * @return {Ti.Filesystem.File} - A reference to a File
 */
exports.getFile = function(path) {
	return Ti.Filesystem.getFile(app_data_directory + path);
};

/**
 * Returns the content of a File.
 * 
 * @param {String} path - The path to a File
 * @return {String} - The content of a File, if it exists
 */
exports.getFileContent = function(path) {
    if (Ti.Filesystem.getFile(app_data_directory + path).exists()) {
        return Ti.Filesystem.getFile(app_data_directory + path).read().text;
    } else {
        return false;
    }
};

/**
 * Deletes a file.
 * 
 * @param {Object} params - Object containing the options to use with this method
 * @param {String} params.path - The path to a file
 * @param {Function} [params.success] - Function to execute in case the file is deleted
 * @param {Function} [params.error] - Function to execute in case the file is deleted
 */
exports.removeFile = function(params) {
    var file = Ti.Filesystem.getFile(app_data_directory + params.path);
    
    if (file.exists() && file.deleteFile()) {
    	params.success && params.success();
    } else {
        params.error && params.error();
    }
    
    file = null;
};

/**
 * Creates a new directory.
 * 
 * @param {Object} params - Object containing the options to use with this method
 * @param {String} params.path - The path of the new directory
 * @param {Function} [params.success] - Function to execute in case the directory is created
 * @param {Function} [params.error] - Function to execute in case the directory is not created
 */
exports.createDirectory = function(params) {
    if (Ti.Filesystem.getFile(app_data_directory + params.path).createDirectory()) {
    	params.success && params.success();
    } else {
        params.error && params.error();
    }
};

/**
 * Deletes a certain directory
 *  
 * @param {Object} params - Object containing the options to use with this method
 * @param {String} params.path - The path of the directory to delete
 * @param {Function} [params.success] - Function to execute in case the directory is deleted
 * @param {Function} [params.error] - Function to execute in case the directory is not deleted
 */
exports.removeDirectory = function(params) {
    var dir = Ti.Filesystem.getFile(app_data_directory + params.path);
    
    if (dir.exists() && dir.isDirectory() && dir.deleteDirectory(true)) {
        params.success && params.success();
    } else {
        params.error && params.error();
    }
    
    dir = null;
};

/**
 * Returns the children files / directories of a certain directory.
 * 
 * @param {String} path - The path of the directory which you want to retrieve the children files / directories
 * @return {String[]} - Array with the contents of this folder
 */
exports.getDirectoryListing = function(path) {
    return Ti.Filesystem.getFile(app_data_directory + path).getDirectoryListing();
};
