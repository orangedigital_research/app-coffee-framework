/**
 * Provides easy access to the Ti.Database module and its commands.
 * @module Database
 */

/** Local reference to a database. */
var db = null;  

/** Installs a database file saved in the user's phone */
exports.install = function(path, name) {
    db = Ti.Database.install(path, name);
};

/** Closes your database reference */
exports.close = function() {
    db.close();
};

/**
 * Reinstalls a database file saved in the user's phone
 * 
 * @param {String} path - The path to the folder where the database file is located
 * @param {String} name - The name of the database file
 */
exports.reinstall = function(path, name) {
    exports.install(path, name);
    db.remove();
    exports.install(path, name);
};

/**
 * Opens a database locally
 * 
 * @param {String} name - The name of the database you wish to open 
 */
exports.open = function(name) {
    db = Ti.Database.open(name);
};

/**
 * Executes a query in your local database and return the ResultSet 
 * 
 * @param {Object} params - Object containing the query properties
 * @param {String} params.sql - The query to execute in your database
 * @param {String[]} params.values - The values referenced in your query (the "?"s)
 * @return {Ti.Database.ResultSet} - Result of the query 
 */
exports.query = function(params) {
    return db.execute(params.sql, params.values);
};

/** 
 * Returns the ID of the last inserted row. 
 * 
 * @return {Integer} - Id of the last inserted row.
 */
exports.getLastRowId = function() {
    return db.getLastInsertRowId();
};

/**
 * Cleans all the data from a certain table
 * 
 * @param {String} table - The name of the table to clean
 */
exports.truncate = function(table) {
    db.execute("DELETE FROM " + table);
};