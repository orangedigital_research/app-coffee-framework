// SYSTEM
// --------------------
// Use this plugin in your app:
//
// `var BGSERVICE = require('/plugin/system/bgservice');`
//
// `BGSERVICE.register('bg_beacon.js');`

/* *********************
 * BACKGROUND SERVICE
 * v 1.0 - iOS 7 Only
 * *********************
 */
var BGSERVICE = {

    // `BGSERVICE.api.*` links to the Titanium Background Services API for iOS
    api: Titanium.App.iOS,

    network: {
        online: Titanium.Network.online,
        type: Titanium.Network.networkType,
    },

    /* ACTIONS */

    // `BGSERVICE.register(url)` register a background service script stored in  /lib/
    //
    // `@input: url (string)`
    register: function(url) {
        BGSERVICE.api.registerBackgroundService({
            url: url //>   in   /app/lib/
        });
    },

    setBgServiceInterval: function(interval) {
        BGSERVICE.api.setMinimumBackgroundFetchInterval(interval);
    },

    // `BGSERVICE.scheduleLocalNotification(param)` schedule a local notification
    //
    // `@input: param (Object)`
    scheduleLocalNotification: function(param) {

        var notification = BGSERVICE.api.scheduleLocalNotification({
            alertBody: param.alertBody,
            alertAction: param.alertAction,
            userInfo: param.userInfo,
            badge: param.badge,
            date: param.date,
            repeat: param.repeat
        });

    },

    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[BGSERVICE] " + string);
                break;

            case "debug":
                Titanium.API.debug("[BGSERVICE] " + string);
                break;

            case "error":
                Titanium.API.error("[BGSERVICE] " + string);
                break;
        }
    },

    /* OBSERVERS */

    observerBgserviceNetwork: function(e) {
        BGSERVICE.log("info", "bgservice:observerNetwork");
        BGSERVICE.network.online = Titanium.Network.online;
        BGSERVICE.network.type = Titanium.Network.networkType;
    },

    observerBgserviceFetch: function(e) {
        BGSERVICE.log("info", "bgservice:observerFetch");
    },

    observerBgserviceTransfert: function(e) {
        BGSERVICE.log("info", "bgservice:observerTransfert");
    },

    observerBgserviceDownloadCompleted: function(e) {
        BGSERVICE.log("info", "bgservice:observerDownloadComplete");
    },

    observerBgserviceDownloadProgress: function(e) {
        BGSERVICE.log("info", "bgservice:observerDownloadProgress");
    },

    observerBgserviceLocalNotification: function(e) {
        BGSERVICE.log("info", "bgservice:observerLocalNotification");
    },

    observerBgserviceSilentPush: function(e) {
        BGSERVICE.log("info", "bgservice:observerSilentPush");
    },
};

/*****************************/

function eventBgserviceNetworkChange(e) {
    BGSERVICE.observerBgserviceNetwork();
};
Titanium.Network.removeEventListener('change', eventBgserviceNetworkChange);
Titanium.Network.addEventListener('change', eventBgserviceNetworkChange);

function eventBgserviceBackgroundFetch(e) {
    BGSERVICE.observerBgserviceFetch(e);
};
BGSERVICE.api.removeEventListener('backgroundfetch', eventBgserviceBackgroundFetch);
BGSERVICE.api.addEventListener('backgroundfetch', eventBgserviceBackgroundFetch);

function eventBgserviceBackgroundTransfert(e) {
    BGSERVICE.observerBgserviceTransfert(e);
};
BGSERVICE.api.removeEventListener('backgroundtransfer', eventBgserviceBackgroundTransfert);
BGSERVICE.api.addEventListener('backgroundtransfer', eventBgserviceBackgroundTransfert);

function eventBgserviceDownloadCompleted(e) {
    BGSERVICE.observerBgserviceDownloadCompleted(e);
};
BGSERVICE.api.removeEventListener('downloadcompleted', eventBgserviceDownloadCompleted);
BGSERVICE.api.addEventListener('downloadcompleted', eventBgserviceDownloadCompleted);

function eventBgserviceDownloadProgress(e) {
    BGSERVICE.observerBgserviceDownloadProgress(e);
};
BGSERVICE.api.removeEventListener('downloadprogress', eventBgserviceDownloadProgress);
BGSERVICE.api.addEventListener('downloadprogress', eventBgserviceDownloadProgress);

function eventBgserviceLocalNotification(e) {
    BGSERVICE.observerBgserviceLocalNotification(e);
};
BGSERVICE.api.removeEventListener('notification', eventBgserviceLocalNotification);
BGSERVICE.api.addEventListener('notification', eventBgserviceLocalNotification);

function eventBgserviceSilentPush(e) {
    BGSERVICE.observerBgserviceSilentPush(e);
};
BGSERVICE.api.removeEventListener('silentpush', eventBgserviceSilentPush);
BGSERVICE.api.addEventListener('silentpush', eventBgserviceSilentPush);

/*****************************/

module.exports = BGSERVICE;