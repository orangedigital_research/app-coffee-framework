// SOCIAL
// --------------------
// Use this plugin in your app:
//
// `var FACEBOOK = require('/plugin/social/facebook');`
//
// `FACEBOOK.int(0000000000);`

/* *********************
 * FACEBOOK
 * v 1.0
 * *********************
 */
var FACEBOOK = {
    permission: ['read_friendlists', 'friends_online_presence'],
    extra: ['publish_actions'],
    api: require('facebook'),

    /* INIT */

    // Initialise plugin and set up the Facebook AppID `FACEBOOK.init(app_id)`
    //
    // `@input: app_id (int)`
    //
    // `@output: (Boolean)` 	
    init: function(app_id) {
        FACEBOOK.api.appid = app_id;

        if (OS_IOS) {
            FACEBOOK.api.permission = FACEBOOK.permission;
            FACEBOOK.api.forceDialogAuth = false;
        } else {
            FACEBOOK.api.permission = FACEBOOK.permission.concat(FACEBOOK.extra);
            FACEBOOK.api.forceDialogAuth = false;
        }

        return FACEBOOK.getAccess();
    },

    network: {
        online: Titanium.Network.online,
        type: Titanium.Network.networkType,
    },

    checkLocalFBID: function() {
        if (FACEBOOK.api.uid == FACEBOOK.getLocalFBID()) {
            return true;
        } else {
            return false;
        }
    },

    getLocalFBID: function() {
        if (Number(Titanium.App.Properties.getString('app_facebook_fbid')) == 1) {
            return true;
        } else {
            return false;
        }
    },

    setLocalFBID: function(app_facebook_fbid) {
        Titanium.App.Properties.setString('app_facebook_fbid', app_facebook_fbid);
    },

    getFBID: function() {
        return FACEBOOK.api.uid;
    },

    getAccess: function() {
        return Number(Titanium.App.Properties.getString('facebook_access'));
    },

    setAccess: function(facebook_access) {
        Titanium.App.Properties.setString('facebook_access', facebook_access);
    },

    getExtraPermission: function() {
        return Number(Titanium.App.Properties.getString('facebook_extra'));
    },

    setExtraPermission: function(facebook_extra) {
        Titanium.App.Properties.setString('facebook_extra', facebook_extra);
    },

    grantAccess: function() {
        FACEBOOK.api.authorize();
    },

    /* ACTIONS */

    // `FACEBOOK.grantPermission(param)` grant extra permission for iOS
    //
    // `@callback: param.success (function)`
    //
    // `@callback: param.error (function)`
    grantPermission: function(param) {
        if (FACEBOOK.getExtraPermission() != 1) {

            if (OS_IOS) {
                FACEBOOK.api.reauthorize(param.permissions, param.scope, function(r) {
                    if (r.success) {
                        FACEBOOK.setExtraPermission(1);
                        if (typeof param.success != "undefined") {
                            param.success();
                        }
                    } else {
                        FACEBOOK.setExtraPermission(0);
                        if (typeof param.error != "undefined") {
                            param.error();
                        }
                    }
                });
            }

            if (OS_ANDROID) {
                FACEBOOK.setExtraPermission(1);
                if (typeof param.success != "undefined") {
                    param.success();
                }
            }
        } else {
            FACEBOOK.setExtraPermission(0);
            if (typeof param.error != "undefined") {
                param.error();
            }
        }
    },

    // `FACEBOOK.getFriends(callback)` get friends list
    //
    // `@callback: callback (function)`
    //
    // `@output: [{facebook.user}]`
    getFriends: function(callback) {
        if (FACEBOOK.api.loggedIn) {
            var param = {
                fields: "picture,username,first_name,last_name,installed"
            };
            var query = "/" + FACEBOOK.api.uid + "/friends";
            FACEBOOK.api.requestWithGraphPath(query, param, "GET", function(e) {
                if (typeof e.result != "undefined") {
                    var fbResult = JSON.parse(e.result);

                    if (typeof callback != "undefined") {
                        callback(fbResult.data);
                    }

                    FACEBOOK.log("info", "facebook:requestFacebookFriends (" + fbResult.data.length + ")");
                    fbResult = null;
                } else {
                    FACEBOOK.log("info", "facebook:requestFacebookFriends ERROR");
                }
            });
            param = null;
        } else {
            FACEBOOK.log("error", "getFriends > Connected: " + FACEBOOK.api.loggedIn);
        }
    },

    // `FACEBOOK.invite(param)` send a request to a friend
    //
    // `@input: param.message (string)`
    //
    // `@input: param.fbid (string)`
    //
    // `@callback: param.success (function)`
    //
    // `@callback: param.error (function)`
    invite: function(param) {
        FACEBOOK.api.dialog("apprequests", {
            message: param.message,
            to: param.fbid
        }, function(e) {
            if (e.success) {
                if (e.result.indexOf('to') > 0) {
                    if (typeof param.success != "undefined") {
                        param.success();
                    }
                } else {
                    if (typeof param.error != "undefined") {
                        param.error();
                    }
                }
            } else {
                if (typeof param.error != "undefined") {
                    param.error();
                }
            }
        });
    },

    // `FACEBOOK.share(param)` post a status, an image etc...
    //
    // `@input: data (object)`
    //
    // `@input: param.graph_url (string)`
    //
    // `@callback: param.success (function)`
    //
    // `@callback: param.error (function)`
    share: function(param, data) {
        if (FACEBOOK.getExtraPermission() == 1) {
            var graph_url = "/" + FACEBOOK.api.uid + "/" + param.graph_url;

            FACEBOOK.log("info", "Share: " + graph_url + " >> " + JSON.stringify(data));
            FACEBOOK.api.requestWithGraphPath(graph_url, data, "POST", function(r) {
                if (r.result) {
                    FACEBOOK.log("info", "Share Succeed > " + data.message);
                    if (typeof param.success != "undefined") {
                        param.success();
                    }

                } else {
                    FACEBOOK.log("error", "Share Failed > " + data.message);
                    if (typeof param.error != "undefined") {
                        param.error();
                    }
                }
            });

        } else {
            FACEBOOK.grantPermission({
                permissions: ["publish_actions"],
                scope: "everyone",
                success: function() {
                    FACEBOOK.share(param, data);
                },
                error: function() {
                    FACEBOOK.log("error", "Share Failed > " + data.message);
                    if (typeof param.error != "undefined") {
                        param.error();
                    }
                }
            });
        }


    },


    /* LOG */

    log: function(type, string) {
        switch (type) {
            case "info":
                Titanium.API.info("[FACEBOOK] " + string);
                break;

            case "debug":
                Titanium.API.debug("[FACEBOOK] " + string);
                break;

            case "error":
                Titanium.API.error("[ ACEBOOK] " + string);
                break;
        }
    },


    /* OBSERVERS */

    observerFacebookLogin: function(e) {
        FACEBOOK.log("info", "facebook:observerLogin > " + JSON.stringify(e));
        if (e.success) {
            FACEBOOK.setAccess(1);
            FACEBOOK.setLocalFBID(FACEBOOK.api.uid);

            if (OS_ANDROID) {
                FACEBOOK.setExtraPermission(1);
            }
            return true;
        } else if (e.error) {
            FACEBOOK.setAccess(0);
            FACEBOOK.setLocalFBID(0);

            if (OS_ANDROID) {
                FACEBOOK.setExtraPermission(0);
            }

            if (FACEBOOK.network.online) {
                var facebook_alt = Ti.UI.createAlertDialog({
                    message: 'This app is not authorised to publish through Facebook:\nSettings > Facebook',
                    ok: 'OK',
                    title: 'Facebook'
                }).show();
                facebook_alt = null;
            }

            return false;

        } else if (e.cancelled) {
            FACEBOOK.setAccess(0);
            FACEBOOK.setLocalFBID(0);

            if (OS_ANDROID) {
                FACEBOOK.setExtraPermission(0);
            }

            return false;
        }
    },

    observerFacebookLogout: function(e) {
        FACEBOOK.log("info", "facebook:observerLogout > " + JSON.stringify(e));
        FACEBOOK.setAccess(0);
        FACEBOOK.setLocalFBID(0);
    },

    observerFacebookNetwork: function(e) {
        FACEBOOK.log("info", "facebook:observerNetwork");
        FACEBOOK.network.online = Titanium.Network.online;
        FACEBOOK.network.type = Titanium.Network.networkType;
    }

};

/*****************************/

function eventFacebookLogin(e) {
    FACEBOOK.observerFacebookLogin(e);
}
FACEBOOK.api.removeEventListener('login', eventFacebookLogin);
FACEBOOK.api.addEventListener('login', eventFacebookLogin);

function eventFacebookLogout(e) {
    FACEBOOK.observerFacebookLogout(e);
}
FACEBOOK.api.removeEventListener('logout', eventFacebookLogout);
FACEBOOK.api.addEventListener('logout', eventFacebookLogout);

function eventFacebookNetwork(e) {
    FACEBOOK.observerFacebookNetwork(e);
}
Titanium.Network.removeEventListener('change', eventFacebookNetwork);
Titanium.Network.addEventListener('change', eventFacebookNetwork);

/*****************************/

module.exports = FACEBOOK;