/**
 * Provides several helper methods to interact with the native contacts book.
 * 
 *  (!) ANDROID PERMISSION IN TIAPP.XML
 *      <uses-permission android:name="android.permission.READ_CONTACTS"/>
 *      <uses-permission android:name="android.permission.WRITE_CONTACTS"/>
 * 
 * @module Contact
 */

/** Regex to validate e-mails */
var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

/** Reference to the Titanium Contacts API */
exports.api = Titanium.Contacts;

/** Initialise plugin and request access to the Address Book exports.init() */
exports.init = function() {
	if (exports.api.hasContactsPermissions() == false) {
    	exports.requestAccess();
    }
};

/** Opens the native Contact settings */
exports.editPermissions = function() {
	if (OS_IOS) {
		Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
	} else {
		var intent = Ti.Android.createIntent({
			action: 'android.settings.APPLICATION_SETTINGS',
		});
		
		intent.addFlags(Ti.Android.FLAG_ACTIVITY_NEW_TASK);
		
		Ti.Android.currentActivity.startActivity(intent);
	}
};

/** Requests the user for permission to use the Calendar */
exports.requestAccess = function() {
	// On iOS we can get information on the reason why we might not have permission
	if (OS_IOS) {
		if (exports.api.contactsAuthorization == exports.api.AUTHORIZATION_DENIED) {
    		exports.editPermissions();
    		return;
    	}
	}
	
	// The new cross-platform way to request permissions
	exports.api.requestContactsPermissions(function(e) {
		if (e.success) {
			Ti.API.info( "Contacts permission granted successfully.");
		} else if (OS_ANDROID) {
			Ti.API.error( "You don't have the required uses-permissions in tiapp.xml or you denied permission for now, forever or the dialog did not show at all because you denied forever before.");
		} else {
			Ti.API.error( "Contacts permission not granted successfully. User has denied permission.");
		}
	});
};


/**
 * Gets the most important data from the user contact book
 * 
 * @return {Array} - Array with the user's contacts data
 */
exports.getAddressBook = function() {
    return exports.getAddressBookFiltered(exports.getAllPerson());
};

/**
 * Search through a list of a contacts and return an array with their most relevant data
 * 
 * @param {Ti.Contacts.Person[]} addressbook - A list of contacts
 * @return {Array} - Array with the user's contacts data 
 */
exports.getAddressBookFiltered = function(addressbook) {
    var list = [];
    
    for (var i = 0, j = addressbook.length; i < j; i++) {
    	var fbusername;
    	
        if (addressbook[i].instantMessage) {
        	_.each(addressbook[i].instantMessage, function(instant_message) {
        		if (instant_message.service.toLowerCase() == 'facebook') {
        			fbusername = instant_message.username;
        		};
        	});
        }

        list.push({
            id: addressbook[c].identifier || addressbook[c].id,
            first_name: addressbook[c].firstName || "",
            last_name: addressbook[c].lastName || "",
            emails: exports.getAllEmails(addressbook[c].email),
            phones: addressbook[c].phone,
            fb: fbusername || null
        });
    }

    return list;
};

/**
 * Displays the native contact picker
 * 
 * @param {Object} params - Parameters for the native contact picker action (select person or cancel)
 * @param {Function} params.selectedPerson - Callback function to be executed when a contact is selected
 * @param {Function} params.cancel - Callback function to be executed when the user cancels the contact picker
 */
exports.pick = function(params) {
    exports.api.showContacts(param);
};

/**
 * Returns all the user's contacts
 * 
 * @return {Ti.Contacts.Person[]} - Array with all the user's contacts
 */
exports.getAllPerson = function() {
    return exports.api.getAllPeople();
};

/**
 * Returns a contact by its ID 
 * 
 * @param {Integer} person_id - The ID of a contact
 * @return {Ti.Contacts.Person} - A Person object
 */
exports.getPerson = function(person_id) {
    return exports.api.getPersonByID(person_id);
};

/**
 * Adds a new contact to the Contact Book
 * 
 * @param {Dictionary<Ti.Contacts.Person>} param - The parameters for the new contact
 * @param {Function} param.callback - Function to be executed after the new contact been created
 * @return {Ti.Contacts.Person} - The newly created Person object
 */
exports.addPerson = function(param) {
    var person = exports.api.createPerson(param.person);
    
    param.callback && param.callback(person);

    return person;
};

/**
 * Removes a contact from the Address Book
 * 
 * @param {Ti.Contacts.Person} param.person - Person to remove from the Address Book
 * @param {Function} param.callback - Callback function to execute after removing this Person
 */
exports.removePerson = function(param) {
    exports.api.removePerson(param.person);
    
    if (OS_IOS) {
        exports.api.save();
    }
    
    param.callback && param.callback();
};


/**
 * Returns all the contact groups.
 * iOS only.
 * 
 * @return {Ti.Contacts.Group[]} - Array with the user's contact groups
 */
exports.getAllGroup = function() {
    if (OS_IOS) {
        return exports.api.getAllGroups();
    }
};

/**
 * Returns a group by its ID
 * iOS only.
 * 
 * @param {Integer} group_id - ID of a group
 * @return {Ti.Contacts.Group} - A contact group
 */
exports.getGroup = function(group_id) {
    if (OS_IOS) {
        return exports.api.getGroupByID(group_id);
    }
};

/**
 * Returns a collection of Persons attached to a Group
 * iOS only
 *  
 * @param {Integer} group_id - ID of a group 
 */
exports.getGroupMembers = function(group_id) {
    if (OS_IOS) {
        return exports.getGroup(group_id).members();
    }
};

/**
 * Creates a new Group in the Address Book.
 * iOS only.
 * 
 * @param {Dictionary<Ti.Contacts.Group>} param.group - The parameter of the new Group
 * @param {Function} [param.callback] - A function to execute after the Group has been created 
 * @return {Ti.Contacts.Group} - The newly created Group
 */
//
exports.addGroup = function(param) {
    if (OS_IOS) {
        var group = exports.api.createGroup(param.group);
        
        exports.api.save();
        
        param.callback && param.callback(group);
     
		return group;
    }
};

/**
 * Removes a Group from the Addres Book
 * 
 * @param {Ti.Contacts.Group} param.group - A Group to be deleted
 * @param {Function} [param.callback] - A function to execute after the Group has been removed 
 */
exports.removeGroup = function(param) {
    if (OS_IOS) {
        exports.api.removeGroup(param.group);
        exports.api.save();
        
        param.callback && param.callback();
    }
};


/**
 * Returns all the e-mails from a certain Person
 * 
 * @param {Object} email - An object with the key:value format with the user's email infos
 * @return {String[]} - Array with the user e-mails 
 */
exports.getAllEmails = function(email) {
    var result = [];
    
    for (var key in object) {
    	// if contact has more than 0 e-mails for work/home/other
		for (var email in object[key]) {
            if (filter.test(object[key][email])) {
                result.push(object[key][email]);
            }
		}
    }
    
    return result;
};