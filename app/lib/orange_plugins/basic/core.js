/**
 * This module provides easy access to some of the most used constants in the Titanium SDK.
 * @module Core Utilities
 */

/** Contains information about the hardware of the device */
exports.device = {
    simulator: false,
    brand: Titanium.Platform.manufacturer,
    model: Titanium.Platform.model,
    battery: {
        level: Titanium.Platform.batteryLevel,
        state: Titanium.Platform.batteryState,
    },
    os: {
        name: Titanium.Platform.osname,
        version: Titanium.Platform.version,
        major: parseInt(Titanium.Platform.version.split(".")[0], 10),
        minor: parseInt(Titanium.Platform.version.split(".")[1], 10),
    },
    ios_smallscreen: Ti.Platform.displayCaps.platformHeight == 480 ? true : false,
    ios_mediumscreen: Ti.Platform.displayCaps.platformHeight == 568 ? true : false,
    ios_largescreen: Ti.Platform.displayCaps.platformHeight == 667 ? true : false,
    ios_xlargescreen: Ti.Platform.displayCaps.platformHeight == 736 ? true : false,
    width:  Ti.Platform.displayCaps.platformWidth,
	height: Ti.Platform.displayCaps.platformHeight,
	density: Ti.Platform.displayCaps.dpi,
	densityFactor: Ti.Platform.name == 'android' ? Ti.Platform.displayCaps.logicalDensityFactor : 1
};

/** Contains information about the Application itself */
exports.app = {
    username: Titanium.Platform.username,
    locale: Titanium.Platform.locale,
    version: Titanium.App.version,
};

/** Contains information about the internet connectivity */
exports.network = {
    ip: Titanium.Platform.address,
    netmask: Titanium.Platform.netmask,
    mac: Titanium.Platform.macaddress,
    online: Titanium.Network.online,
    type: Titanium.Network.networkType,
};

exports.init = function(){

    if (Titanium.Platform.model == 'google_sdk' || Titanium.Platform.model == 'Simulator') {
        exports.device.simulator = true;
    } else {
        exports.device.simulator = false;
    }

    if(exports.densityFactor > 1){
        exports.width = Math.round(exports.width / exports.densityFactor);
        exports.height = Math.round(exports.height / exports.densityFactor);
    }
    
};

exports.init();


