/**
 * Provides several helper methods to interact with the native calendar App.
 * 
 *  (!) ANDROID PERMISSION IN TIAPP.XML
 *      <uses-permission android:name="android.permission.READ_CALENDAR"/>
 *      <uses-permission android:name="android.permission.WRITE_CALENDAR"/>
 * 
 * @module Calendar
 */


/**
 * Reference to the Titanium Calendar API 
 * @property 
 */
exports.api = Titanium.Calendar;

/** Keeps track of the current selected calendar */
exports.selectedCalendar = null;


/** Initialize the module and requests access to the Calendar */
exports.init = function() {
    // Check if the user has allowed the system to get his calendar details
    // If the Android version is prior than 6.0 than hasCalendarPermissions will return FALSE
    if (exports.api.hasCalendarPermissions()) {
        exports.selectCalendar(exports.getAllCalendar()[0]);
    } else {
        exports.requestAccess();
    }

};
    
/** Opens the native Calendar settings */
exports.editPermissions = function() {
	if (OS_IOS) {
		Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
	} else {
		var intent = Ti.Android.createIntent({
			action: 'android.settings.APPLICATION_SETTINGS',
		});
		
		intent.addFlags(Ti.Android.FLAG_ACTIVITY_NEW_TASK);
		
		Ti.Android.currentActivity.startActivity(intent);
	}
};

/** Requests the user for permission to use the Calendar */
exports.requestAccess = function() {
	// On iOS we can get information on the reason why we might not have permission
	if (OS_IOS) {
		if (exports.api.calendarAuthorization == exports.api.AUTHORIZATION_DENIED) {
    		exports.editPermissions();
    		return;
    	}
	}
	
    // The new cross-platform way to request permissions
	exports.api.requestCalendarPermissions(function(e) {
		if (e.success) {
			Ti.API.info("Calendar permission granted successfully.");
		} else if (OS_ANDROID) {
			Ti.API.error("You don't have the required uses-permissions in tiapp.xml or you denied permission for now, forever or the dialog did not show at all because you denied forever before.");
		} else {
			Ti.API.error("Calendar permission not granted successfully. User has denied permission.");
		}
	});
};


/**
 * Returns a collection with all of the user's calendars
 * 
 * @return {Ti.Calendar.Calendar[]} - Collection with all of the user's calendars
 */
exports.getAllCalendar = function() {
    return exports.api.allCalendars;
};

/**
 * Returns a Ti.Calendar.Calendar
 * 
 * @param {Integer} calendar_id - The ID of a calendar.
 * 
 */
exports.getCalendar = function(calendar_id) {
    return exports.api.getCalendarById(calendar_id);
};

/**
 * Returns the user default calendar
 * 
 * @return {Ti.Calendar.Calendar} - A calendar
 */
exports.getCalendarDefault = function() {
    return exports.api.getDefaultCalendar();
};

/**
 * Sets the exports.selectedCalendar value
 * 
 * @param {Ti.Calendar.Calendar} calendar - A calendar object which will be referenced by exports.selectedCalendar
 */
exports.selectCalendar = function(calendar) {
    exports.selectedCalendar = calendar;
};

/**
 * Gets an event by its ID
 * 
 * @param {Integer} event_id - An event ID
 * @return {Ti.Calendar.Event} - An Event object
 */
exports.getEvent = function(event_id) {
    return exports.selectedCalendar.getEventById(event_id);
};

/**
 * Adds an event into the currently selected calendar
 * 
 * @param {Ti.Calendar.Event} event_param - A Event object
 * @return {Integer} - The ID of the newly created Event
 */
exports.addEvent = function(event_param) {
    var evt = exports.selectedCalendar.createEvent(event_param);

    if (OS_IOS) {
        evt.save(exports.api.SPAN_THISEVENT);
    }

    var eid = evt.id;
    return eid;
};

/**
 * Adds an Alert to an Event
 * 
 * @param {Integer} event_id - The ID of an Event
 * @param {Dictionary<Ti.Calendar.Alert>} alert_param - Alert properties which will be used to create the new Alert.
 */
exports.setAlert = function(event_id, alert_param) {
    var evt = exports.getEvent(event_id);
    
    evt.createAlert(alert_param);

    if (OS_IOS) {
        evt.save(exports.api.SPAN_THISEVENT);
    }
};

/**
 * Adds an Reminder to an Event
 * 
 * 
 * @param {Integer} event_id - The ID of an Event
 * @param {Dictionary<Ti.Calendar.Reminder>} reminder_param - Reminder properties which will be used to create the new Reminder.
 */
exports.setReminder = function(event_id, reminder_param) {
	if (OS_ANDROID) {
	    var evt = exports.getEvent(event_id);
	    
	    evt.createReminder(reminder_param);
	}
};

/**
 * Creates an recurrence pattern for a recurring event
 * 
 * @param {Integer} event_id - The ID of an Event
 * @param {Dictionary<Ti.Calendar.RecurrenceRule>} recurrent_param - RecurrenceRule properties which will be used to create the new RecurrenceRule.
 */
exports.setRecurrent = function(event_id, recurrent_param) {
    if (OS_IOS) {
        var evt = exports.getEvent(event_id);
        
        evt.createRecurrenceRule(recurrent_param);

        evt.save(exports.api.SPAN_THISEVENT);
    }
};