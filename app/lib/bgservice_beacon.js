/* *********************
 * BEACON BKGRD SERVICE
 * v 1.0
 * *********************
 */ 
 
var BEACON              = require("orange_plugins/location/beacon");
var BGSERVICE           = require("orange_plugins/system/bgservice");
var beacon_param        = BEACON.getBeacon();


function beacon_background_advertise(data){
    BEACON.log("debug","beacon_adverstise > "+ JSON.stringify(data.status) );
}

function beacon_background_enterRegion(data){
    BEACON.log("debug","beacon_enterRegion > "+ JSON.stringify(data) );
}

function beacon_background_exitRegion(data){
    BEACON.log("debug","beacon_exitRegion > "+ JSON.stringify(data) );
}

function beacon_background_stateRegion(data){
    BEACON.log("debug","beacon_stateRegion > "+ JSON.stringify(data) );
}

function beacon_background_range(data){
    BEACON.log("debug","beacon_range > "+ JSON.stringify(data.beacons) );
    for(var i in data.beacons) {
        var b = data.beacons[i];
        var beacon_identifier   = BEACON.getBeaconIdentifier(b);
        
        switch (b.proximity) {
            
            case "immediate":
                var message = "Great, you find "+beacon_identifier+"!";  
                beacon_background_notification(beacon_identifier, message);              
            break;
            
            case "near":
                var message = beacon_identifier+" is near";
                beacon_background_notification(beacon_identifier, message);
            break;
            
            case "far":
                var message = beacon_identifier+" is far";
                beacon_background_notification(beacon_identifier, message);
            break;
            
        }
        
        
    }    
}

function beacon_background_proximity(data){
    BEACON.log("debug","beacon_proximity > "+ JSON.stringify(data) );
}

function beacon_background_notification(identifier, message){    
    var param = {
            alertBody : message,
            alertAction : "OK",
            userInfo : {
                uuid:identifier
            },
            badge : 0,
            date : new Date(new Date().getTime() + 10),
            repeat : ""
    };
    BGSERVICE.scheduleLocalNotification(param);
}

var settings = {
    autoRanging:true,       
    advertise: function(e){
        beacon_background_advertise(e);
    },          
    enteredRegion: function(e){
        beacon_background_enterRegion(e);
    },
    exitedRegion: function(e){
        beacon_background_exitRegion(e);
    },
    determinedRegionState: function(e){
        beacon_background_stateRegion(e);
    },
    ranges: function(e){
        beacon_background_range(e);
    },               
    proximity: function(e){
        beacon_background_proximity(e);
    } 
    
};

BEACON.init(settings);
BEACON.startScanner(beacon_param);



Ti.App.currentService.addEventListener("stop", function() {
    BEACON.stopScanner();
});